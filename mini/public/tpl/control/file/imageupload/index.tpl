<div>
	<div class="file-imageupload">
		<div class="img-upload-wrap">
			<img src="${url}" alt="" class="cover-image"/>
			<div class="imgupload-btn">

				<div class="imgupload-plus">
					<div class="imgupload-remove-btn-wrap">
						<div class="imgupload-remove-btn ${url?'':'hidden'}">
							移除
						</div>
					</div>
					<div class="imgupload-hint-text">
						${hint}
					</div>
				</div>
				<form>
					<input u-change="onFileChange" type="file" accept="image/*"/>
				</form>
			</div>
		</div>

	</div>
	
</div>