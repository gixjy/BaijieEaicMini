$.app({
	data:{
		search_text:"",
		placeholder:"搜索"
	},
	onLoad:function(){

	},
	onKey:function (e) {
		var me = this;
		$.log("::::: search onKey:"+e.keyCode+" searchtext:"+me.data.search_text);
		if( $.toInt(e.keyCode) === 13 )
            me.setData({search_text:me.data.search_text});
    },
	onSearch:function(e){
		var me = this;
		$.log("onsearch-->"+me.data.search_text);
		me.setData({search_text:me.data.search_text});
	}
});