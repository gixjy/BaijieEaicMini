<div>
  <div class="field has-addons">
    <div class="control">
      <input type="text" class="input" placeholder="${placeholder}" u-v="search_text" u-change="onSearch"/>
    </div>
    <div class="control">
      <a class="button is-primary" u-click="onSearch">
        <i class="fa fa-search"></i>
      </a>
    </div>
  </div>



</div>