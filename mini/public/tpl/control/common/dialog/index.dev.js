eui.app({
    data:{
        dialog:{
            type:"",
            title:"",
            text:"",
            fnOK:null,
            fnCancel:null
        }
    },
    onLoad:function () {
        var me = this;

        me.setupDialog();
    },
    onLoadChild:function (name,childapp) {
        var me = this;

    },
    onCancel:function (e) {
        var me = this;
        if( me.data.dialog.type ){
            var app = me.children[me.data.dialog.type];
            app.onCancel(e);
        }

    },
    onRemoveChild:function (name,childapp) {
        var me = this;
    },
    setupDialog:function () {
        var me = this;

        var cfg = eui.getConfig();
        eui.each(["alert","warning","error","confirm"],function(v){
            cfg[v] = function (title,text,fnOK,fnCancel) {
                me.setData({
                    dialog:{
                        type:v,
                        title:title,
                        text:text,
                        fnOK:fnOK,
                        fnCancel:fnCancel
                    }
                });
            };
        });

    }
});