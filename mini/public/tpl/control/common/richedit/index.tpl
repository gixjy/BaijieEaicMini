<div>
    <div class="common-richedit">
        <div class="richedit-toolbar richedit-toolbar-${toolbarVisible}">
            <a class="richedit-btn" u-click="onBold" title="粗体">
                <i class="fa fa-bold"></i>
            </a>
            <a class="richedit-btn" u-click="onItalic" title="斜体">
                <i class="fa fa-italic"></i>
            </a>
            <a class="richedit-btn" u-click="onUnderline" title="下划线">
                <i class="fa fa-underline"></i>
            </a>
            <a class="richedit-btn" u-click="onStrikeout" title="删除线">
                <i class="fa fa-strikethrough"></i>
            </a>
            <a class="richedit-btn lm-n" u-click="onListOl" title="有序列表">
                <i class="fa fa-list-ol"></i>
            </a>
            <a class="richedit-btn" u-click="onListUl" title="无序列表">
                <i class="fa fa-list-ul"></i>
            </a>
            <a class="richedit-btn lm-n" u-click="onParagragh" title="段落">
                <i class="fa fa-paragraph"></i>
            </a>
            <a class="richedit-btn lm-n" u-click="onHorzLine" title="水平线">
                <i class="fa fa-window-minimize"></i>
            </a>
            <a class="richedit-btn lm-n" u-click="onQuote" title="引用">
                <i class="fa fa-quote-left"></i>
            </a>
            <a class="richedit-btn lm-n" u-click="onCode" title="代码">
                <i class="fa fa-keyboard-o"></i>
            </a>
        </div>
        <div class="richedit-content"
             style="${height? 'height:'+height+';overflow:auto;':''}"
             u-dom="contentDom"
             u-keypress="onKeypress"
             u-blur="onBlur"
             u-v="value"
             contenteditable="true">
        </div>
    </div>
</div>