<div>

    <div class="dropdown is-hoverable is-right">
        <div class="dropdown-trigger">
            <button class="button is-text" aria-haspopup="true" >
                <i class="fa fa-list-ul" ></i>
            </button>
        </div>
        <div class="dropdown-menu"  role="menu">
            <div class="dropdown-content">
                <a data-index="${index}" u-click="onToggle"
                   class="dropdown-item" u-each="cols" u-item="col" u-index="index">
                    <i class="fa fa-${col.visible ? 'check-':''}square-o rp-m"></i>
                    ${col.text}
                </a>
            </div>
        </div>
    </div>


</div>