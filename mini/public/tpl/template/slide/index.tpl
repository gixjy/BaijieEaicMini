<div>
	<div class="slide-container">
		<div u-each="slides" u-item="slide" class="slide-item ${slide.id==activeid ? '':'hidden'}">
			<img src="${slide.img}" class="cover-image"/>
		</div>
	</div>
</div>