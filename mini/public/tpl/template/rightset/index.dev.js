eui.app({
    data:{
        rights:[],
        curright:[]
    },
    onLoad:function () {
        
    },
    onCheck:function (e) {
        var me = this;

        var thisDom = eui.dom(e.src);
        var isleaf = thisDom.hasClass("right-leaf");
        var rightname = thisDom.data("item");

        eui.log("click on "+rightname+" isleaf:"+isleaf);
        //bjui.thisDom = thisDom;

        if( thisDom.find(".fa").hasClass("fa-check-square-o") ){
            if( isleaf ) me.removeRight(rightname);
            else me.removeRightParent(rightname);
        }
        else{
            if( isleaf ) me.addRight(rightname);
            else me.addRightParent(rightname);
        }
    },
    convertRight:function(path,rights,curright){
        var me = this;

        var ret=[];
        eui.each(rights,function(v,k){
            var obj = {};
            obj.name = k;
            obj.path = (path != "" ? path + "_":"") + k;

            if( eui.isArray(v) ){
                var arr=[];
                eui.each(v,function(item){
                    var objChild={};
                    arr.push( objChild );

                    objChild.name = item;
                    objChild.path = obj.path + "_" + item;
                    objChild.checked = (eui.inArray(objChild.path,curright) ? 1 : 0);
                    //eui.log("in array ? "+objChild.path +" ret:"+ (objChild.checked ? "yes":"no") );
                });
                obj.children = arr;
                obj.children_leaf = true;
            }
            else if( eui.isObject(v) ){
                obj.children = me.convertRight(obj.path,v,curright);
                obj.children_leaf = false;
            }
            ret.push( obj );
        });
        return ret;
    }
});