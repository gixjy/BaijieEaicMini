eui.app({
    data:{
        //page_index data_count page_count
        pagerange : function ($pageindex, $pagecount) {
            if ($pagecount < 1) return [];
            var delta=3;
            var $start = ($pageindex > delta ? $pageindex - delta : 1);
            var $end = ($pageindex + delta <= $pagecount ? $pageindex + delta : $pagecount);
            var $arr = [];
            if ($end >= $start) {
                for (var $index = $start; $index <= $end; $index++)
                    $arr.push($index);
            }
            return $arr;
        },
        pageindex:1,
        pagecount:0,
        datacount:0
    },

    go_page:function (e) {
        this.setData({
            pageindex : eui.toInt( eui.dom(e.src).get("pageindex"))
        })
    },
    onPrevPage:function (e) {
        var me = this;
        if( me.data.pageindex >1 ) me.setData({
            pageindex : me.data.pageindex-1
        });
    },
    onNextPage:function (e) {
        var me = this;
        if( me.data.pageindex < me.data.pagecount ) me.setData({
            pageindex : me.data.pageindex+1
        });
    }

});