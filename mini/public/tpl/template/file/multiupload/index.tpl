<div>
    <div class="multiupload columns is-mobile">
        <div class="column is-one-quarter" u-each="images" u-item="image" u-index="index">
            <div class="pre-image">
                <img class="cover-image " src="${image.src}"/>
            </div>
            <button class="delete is-small" data-index="${index}" u-click="onRemove"></button>
        </div>
        <div class="column is-one-quarter" u-if="{|images.length<maxcount|}">
            <div class="add-button flex-center">
                <div class="flex-middle width-full">
                    <div class="flex-tb flex-middle width-full">
                        <div class="flex-center">+</div>
                        <div class="flex-center">添加图片</div>
                    </div>
                </div>
                <div class="form-wrap">
                    <form>
                        <input u-change="onFileChange" type="file" accept="image/*" />
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>