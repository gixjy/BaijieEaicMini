$.app({
	data: {
		file: null,
		url:"",
		config: {},
		hint:"点击添加图片"
	},
	//如果要只支持拍照上传，则加上capture="camera"
	/**
	 * <input type="file" accept="image/*" capture="camera">
		<input type="file" accept="video/*" capture="camcorder">
		<input type="file" accept="audio/*" capture="microphone">
		capture表示，可以捕获到系统默认的设备，比如：camera--照相机；camcorder--摄像机；microphone--录音。

		accept表示，直接打开系统文件目录。

		其实html5的input:file标签还支持一个multiple属性，表示可以支持多选，如：

		<input type="file" accept="image/*" multiple>
		加上这个multiple后，capture就没啥用了，因为multiple是专门yong用来支持多选的。
	 */

	updateFile: function (file) {
		var me = this;


	},
	onFileChange: function (e) {
		var me = this;

		var file = (e.srcElement && e.srcElement.files ? e.srcElement.files[0] : (e.src && e.src.Node && e.src.Node.files ? e.src.Node.files[0] : null));

	
		var fnFindImage = function () {
			$.finde = e;
			var parent = e.src.parentNode.parentNode.parentNode;
			var nodecount = parent.childNodes.length;
			for (var i = 0; i < nodecount; i++) {
				var node = parent.childNodes[i];
				if (node.nodeType === 1 && node.tagName.toLowerCase() === 'img') {
					return node;
					//break;
				}
			}
			return null;
		};
		var image = fnFindImage();


		var img = new Image();
		img.onload = function () {
			// 图片原始尺寸
			var originWidth = this.width;
			var originHeight = this.height;
			// 最大尺寸限制
			var maxWidth = 1420;
			var maxHeight = 1100;
			// 目标尺寸
			var targetWidth = originWidth;
			var targetHeight = originHeight;
			// 图片尺寸超过400x400的限制
			if (originWidth > maxWidth || originHeight > maxHeight) {
				if (originWidth / originHeight > maxWidth / maxHeight) {
					// 更宽，按照宽度限定尺寸
					targetWidth = maxWidth;
					targetHeight = Math.round(targetWidth * (originHeight / originWidth));
				} else {
					targetHeight = maxHeight;
					targetWidth = Math.round(targetHeight * (originWidth / originHeight));
				}
			}
			image.width = targetWidth;
			image.height = targetHeight
		};

		var reader = null;
		if( window.FileReader ){
            reader = new FileReader();
            reader.onload = function (e) {
                //$.teste = e;
                img.src = e.target.result;
                image.src = e.target.result
            };
		}



		// 选择的文件是图片
		if (file.type.indexOf("image") === 0) {
			me.setData({ file: file,hint:"点击替换图片" });
			if( reader ) reader.readAsDataURL(file);
		}
	}
});