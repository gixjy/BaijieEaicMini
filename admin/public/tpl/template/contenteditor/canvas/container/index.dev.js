$.app({
    data: {
        entities: [],
        overindex: -1,
        focusindex: -1,
        maxid: 0,
        paper:{}
    },
    on: {
        action: {
            "removeEntity": function (entity) {
                var me = this;
                var index = $.indexOfArray(entity, me.data.entities, function (a, b) {
                    return $.toInt(a.id) === $.toInt(b.id);
                });
                if (index >= 0) {
                    me.data.entities.splice(index, 1);
                    me.setData();
                }
            },
            'focusEntity': function (entity) {
                var me = this;
                var focusIndex = $.indexOfArray(entity, me.data.entities, function (a, b) {
                    return a && a.id === b.id;
                });
                $.log("焦点实体 index:" + focusIndex);
                me.setData({
                    focusindex: focusIndex
                })
            },
            'entityPropChange':function (entity) {
                var me=this;
                var index = $.indexOfArray(entity,me.data.entities,function (a,b) {
                    return a.id === b.id;
                });
                if( index >= 0 ){
                    me.data.entities[index] = entity;
                }
                me.setData();
            }
        }
    },
    onLoad: function () {
        var me = this;
        $.testEntities = me.data.entities;
    },
    //================ container drag
    onContainerDragOver: function (e) {
        var me = this;
        e.stop();

        return true;
    },
    onContainerDrop: function (e) {
        var me = this;
        e.stop();
        $.log('容器Drop...');
        me.setData({
            overindex: -1
        })
    },
    onContainerDragLeave: function (e) {
        var me = this;
        e.stop();
        $.log('容器DragLeave...');
        //me.setData({
        //   overindex:-1
        //})
    },
    //============= append drag
    onAppendDrop: function (e) {
        var me = this;
        e.stop();

        var widget = e.dataTransfer.getData("Widget");
        if (widget) {
            me.moveWidget(me.data.entities.length, widget);
        }
    },
    onAppendDragOver: function (e) {
        var me = this;

        me.setData({
            overindex: me.data.entities.length
        })
    },
    onAppendDragLeave: function (e) {
        var me = this;

    },
    //================== entity drag
    onEntityDragOver: function (e) {
        var me = this;
        var index = $.toInt(e.data.index);

        $.log('实体DragOver index=' + index + '...');
        me.setData({
            overindex: index
        })
    },
    onEntityDragLeave: function (e) {
        var me = this;
        var index = $.toInt(e.data.index);

    },
    onEntityDrop: function (e) {
        var me = this;
        var index = $.toInt(e.data.index);

        $.log('实体drop...');
        me.setData({
            overindex: -1
        })
    },
    onEntityDragStart: function (e) {
        var me = this;

        var index = $.toInt(e.data.index);
        var entity = me.data.entities[index];

        e.dataTransfer.setData("Widget", $.toJson(entity));
    },
    //============= 插入器
    onInsertorDrop: function (e) {
        var me = this;
        var index = $.toInt(e.data.index);
        e.stop();

        var widget = e.dataTransfer.getData("Widget");
        if (widget) {
            //$.log("播入器新增元件："+widget);
            me.moveWidget(index, widget);
        }
    },
    onClick: function (e) {
        var me = this;
        var index = $.toInt(e.data.index);
        var entity = me.data.entities[index];
        //$.focusEntity = entity;
        me.on.action.focusEntity.call(me, entity);
        me.setData({
            action: {
                'focusEntity': entity
            }
        });
        return e.stop();
    },
	onPaperTitleClick: function (e) {
		var me = this;
		var entity = {
			type:"papertitle",
            paper: me.data.paper,
            prop:"/template/contenteditor/widgets/prop/papertitle/index"
		};

		me.setData({
			action: {
				'focusEntity': entity
			}
		});
		return e.stop();
	},
	onPaperIntroClick: function (e) {
		var me = this;

		var entity = {
			type:"paperintro",
			paper: me.data.paper,
			prop:"/template/contenteditor/widgets/prop/paperintro/index"
		};

		me.setData({
			action: {
				'focusEntity': entity
			}
		});
		return e.stop();
	},
    moveWidget: function (index, widget) {
        var me = this;

        if (widget) {
            //$.log("insertdrop:"+widget+" index:"+index);
            var widgetObj = $.fromJson(widget);

            var widgetID = $.toInt(widgetObj.id);
            var entities = me.data.entities;

            if (widgetID > 0) {
                if (!(
                        index >= 0 &&
                        index < entities.length &&
                        widgetID === $.toInt(entities[index].id)))
                {
                    var findIndex = $.indexOfArray(widgetObj, entities, function (a, b) {
                        return widgetID === $.toInt(b.id);
                    });

                    //先删除
                    if (findIndex >= 0) me.on.action.removeEntity.call(me, widgetObj);
                    else {
                        me.setData({
                            action: {
                                removeEntity: widgetObj
                            }
                        });
                    }

                    //再插入
                    if (index >= entities.length) entities.push(widgetObj);
                    else entities.splice(index, 0, widgetObj);
                    me.setData({
                        overindex: -1,
                        entities: entities,
                        action: {
                            focusEntity: widgetObj
                        }
                    });
                }
            }
            else {
                //创建实例
                var entity = $.extend({}, widgetObj);
                entity.id = me.data.maxid++;

                $.log("新增元件："+$.toJson(entity)+" index:"+index+" curlen:"+entities.length);


                if (index >= entities.length)
                    entities.push(entity);
                else
                    entities.splice(index, 0, entity);

                //$.log("insert entity:"+$.toJson(entity));

                me.setData({
                    overindex: -1,
                    entities: entities,
                    maxid: me.data.maxid,
                    focusEntity: entity
                });
            }

        }
    },
    onClickAppend: function (e) {
        var me = this;
        me.setData({
            action: {
                focusEntity: null
            }
        })
    },
    onRemove: function (e) {
        var me = this;
        var index = $.toInt(e.data.index);
        var entities = me.data.entities;
        entities.splice(index, 1);
        me.setData({
            entities: entities,
            action: {
                focusEntity: null
            }
        })
    }
});