<div>
    <div class="paper-title" u-click="onPaperTitleClick">
        ${paper.title || '[请填写标题]'}
    </div>
    <div class="paper-intro" u-click="onPaperIntroClick">
        ${paper.intro || '[请填写简介]'}
    </div>
    <div
         u-dragover="onContainerDragOver"
         u-dragleave="onContainerDragLeave"
    u-drop="onContainerDrop">
        <div u-each="entities"
             u-item="entity"
             u-index="index"
             u-key="${entity.id}"
             draggable="true"
             data-index="${index}"
            class="entity-container"
             u-dragstart="onEntityDragStart"
        >
            <div class="remove-entity ${ {|focusindex==index ? '' : 'is-hidden' |} }">
                <i class="fa fa-remove" u-click="onRemove" data-index="${index}" title="删除"></i>
            </div>
            <div class="entity-insertor ${overindex==index ? '' : 'is-hidden'}"
                 u-drop="onInsertorDrop"
                 data-index="${index}">
                插入到此处
            </div>
            <div class="entity-wrap">
                <div class="serial">
                    ${index+1}.
                </div>
                <div class="control">
                    <div class="{| content-editor-entity ${overindex==index ? 'dragover':''} ${focusindex==index ? 'active':''} |}"
                         u-view="${entity.templ}"
                         u-share="{
                             'entity':'entities[${index}]',
                             'action.focusEntity':'action.focusEntity',
                             'action.entityPropChange':'action.entityPropChange'}"
                         u-dragover="onEntityDragOver"
                         u-dragleave="onEntityDragLeave"
                         u-drop="onEntityDrop"
                         u-click="onClick"

                         data-index="${index}">

                    </div>
                </div>
            </div>

            <div class="entity-insertor ${overindex==index ? '' : 'is-hidden'}"
                 u-drop="onInsertorDrop"
                 data-index="${index+1}">
                插入到此处
            </div>
        </div>
        <div  class="{| text-center content-editor-append ${overindex>=0 && overindex==entities.length ? 'dragover' :''} |}"
              u-click="onClickAppend"
              u-dragleave="onAppendDragLeave"
              u-dragover="onAppendDragOver"
              u-drop="onAppendDrop">
            + 拖放控件至此处以添加
        </div>
    </div>
</div>