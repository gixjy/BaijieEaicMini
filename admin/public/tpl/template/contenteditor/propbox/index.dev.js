$.app({
    data:{
        entity:null,
        empty:{
            title:"没有选中项",
            image:"/tpl/theme/default/images/nodata.png"
        }
    },
    on:{
        action:{
            'focusEntity':function (entity) {
                var me = this;

                /*var views=[];
                if( entity ) views.push(entity);

                $.propFocusEntity = entity;
                $.log("on prop focuschange: views:"+$.toJson(views));
                me.setData({
                    views:views
                });*/
                me.setData({entity:entity});
            }
        }
    }
});