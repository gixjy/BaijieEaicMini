<div>
    <div u-if="entity">
        <div u-view="${entity.prop}" u-share="{
        'entity':'entity',
        'action.entityPropChange':'action.entityPropChange'}">

        </div>
    </div>
    <div u-else="" u-view="/control/common/empty/index" u-share="{'title':'empty.title','image':'empty.image'}">

    </div>



</div>