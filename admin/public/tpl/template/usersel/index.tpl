<div >

    <div class="">
        <div class="vm-n level">
            <div class="level-left">
                <div class="level-item">
                    <div class="buttons">
                        <button class="button is-primary" u-click="onAdd">
                            <i class="fa fa-plus bj-rmxs"></i>
                            添加
                        </button>
                        <button class="button is-danger" u-click="onBatchRemove" >
                            <i class="fa fa-trash bj-rmxs"></i>
                            批量删除
                        </button>
                    </div>
                </div>

            </div>
            <div class="level-right">
                <div class="level-item">
                    <div u-view="/control/form/search/index" u-share="{'search_text':'meta.cond.search_text'}"></div>
                </div>

            </div>
        </div>
        <div class="width-400">

            <div u-if="meta.datas">
                <table  class="table is-striped is-bordered is-hoverable ">
                    <thead class="thead-light">
                    <tr>

                        <th>序号</th>
                        <th>姓名</th>
                        <th>登录名</th>
                        <th>性别</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr u-each="meta.datas" u-item="item" u-key="${item.id}" u-index="index">

                        <td>${index+1}</td>
                        <td>
                            ${item.name}
                        </td>
                        <td>
                            ${item.loginname}
                        </td>
                        <td>
                            ${ $.toInt(item.sex) ? "女":"男" }
                        </td>
                        <td>
                            <span class="has-text-${ $.toInt(item.isvalid) ? 'success':'danger' }">${ $.toInt(item.isvalid) ? "有效":"禁用" }</span>
                        </td>

                        <td>
                            <a type="button" class="button is-small"  u-click="onSelUser" data-index="${index}">选择</a>

                        </td>
                    </tr>

                    </tbody>
                </table>
                <div u-if="meta.pagecount" class="clearfix" >
                    <div u-view="/template/pager/index"
                         u-share="{'pageindex':'meta.cond.pageindex','pagecount':'meta.pagecount','datacount':'meta.datacount'}"></div>

                </div>

            </div>

            <div u-else="">
                <div u-view="/control/common/empty/index" u-share="{'title':'empty.title','image':'empty.image'}"></div>

            </div>

        </div>
    </div>
</div>