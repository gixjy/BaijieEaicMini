$.app({
    data:{
        apis:[],//所有可设置的权限
        curright:{}//设置前已有权限
    },
    onLoad:function () {
        var me = this;

        var curright = me.data.curright;

        $.curright = curright;
        //初始化checkall & checkmodule
        var allcheck = true;
        $.each(me.data.apis,function (api) {
            var name = api.name;
            var groupchecked = true;
            $.each(api.group,function (groupname) {
                var rightname = me.makeRightName(name,groupname);
                if( !curright[rightname] ){
                    groupchecked = false;
                    allcheck = false;
                    //return true;
                }
            });

            curright[ me.makeRightNameModule(name) ] = groupchecked;
        });
        curright[me.makeRightNameAll()] = allcheck;
        me.setData();
    },
    onCheckAll:function(e){
        var me = this;
        var curright = me.data.curright;

        var checked = !curright[ me.makeRightNameAll() ];

        me.updateStatusByAll(checked);

        var rightnames=[];
        $.each(me.data.apis,function (api) {
            $.each(api.group,function (groupname) {
                var rightname = me.makeRightName(api.name,groupname);
                rightnames.push( rightname );
            })
        });
        me.setData({
            action:{
                checkRights:{
                    rightnames : $.join(rightnames,"|"),
                    checked : checked
                }
            }
        })
    },
    onCheckGroup:function(e){
        var me = this;
        var name = e.data.name;
        var group = e.data.group;
        var rightname = me.makeRightName(name,group);
        var checked = !me.data.curright[rightname];

        me.updateStatusByRight(name,group,checked);

        me.setData({
            action:{
                checkRight:{
                    rightname:rightname,
                    checked:checked
                }
            }
        });

    },
    onCheckModule:function(e){
        var me = this;

        var name = e.data.name;
        var checked = !me.data.curright[ me.makeRightNameModule(name) ];


        var rightnames=[];

        $.each(me.data.apis,function (api) {
            if( api.name === name ){
                //
                $.each(api.group,function (groupname) {
                    rightnames.push( me.makeRightName(name,groupname));
                });
                return true;
            }
        });

        me.updateStatusByModule(name,checked);

        me.setData({
            action:{
                checkRights:{
                    rightnames: $.join(rightnames,"|"),
                    checked:checked
                }
            }
        });
    },
    //构造权限名
    makeRightName:function(modulename,groupname){
        return modulename + "_" + groupname;
    },
    makeRightNameModule:function(name){
        return "$"+name;
    },
    makeRightNameAll:function(){
        return "#all";
    },
    //根据权限的勾选，同步模块和全选的状态
    updateStatusByRight:function (name,group,checked) {
        var me = this;
        
        var allcheck = true;
        var modulecheck=true;
        var curright = me.data.curright;

        curright[me.makeRightName(name,group)] = checked;
        
        $.each(me.data.apis,function (api) {
            $.each(api.group,function (groupname) {
                var rightname = me.makeRightName(name,groupname);
                var check2 = !!curright[rightname];
                if( api.name === name && groupname === group ) check2 = checked;
                
                if( name === api.name && !check2 ) modulecheck = 0;
                if( !check2 ) allcheck = false;
            })
        });
        

        curright[me.makeRightNameAll()] = allcheck;
        curright[me.makeRightNameModule(name)] = modulecheck;
    },
    //根据模块的勾选，同步权限和全选的状态
    updateStatusByModule:function (name,checked) {
        var me = this;

        var curright = me.data.curright;

        var allcheck = true;
        $.each(me.data.apis,function (api) {
            $.each(api.group,function (groupname) {
                var rightname = me.makeRightName(api.name,groupname);
                if( name === api.name ){
                    curright[rightname] = checked;
                }
                if( !curright[rightname]) allcheck = false;
            })
        });
        curright[ me.makeRightNameAll() ] = allcheck;
        curright[ me.makeRightNameModule(name)] = checked;
    },
    //根据全选的勾选，同步权限和模块选择的状态
    updateStatusByAll:function (checked) {
        var me = this;

        var curright = me.data.curright;

        curright[ me.makeRightNameAll() ] = checked;

        $.each(me.data.apis,function (api) {
            curright[ me.makeRightNameModule(api.name)] = checked;
            $.each(api.group,function (groupname) {
                var rightname = me.makeRightName(api.name,groupname);
                curright[rightname] = checked;
            });
        });

    }
});