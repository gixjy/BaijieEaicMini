(function () {
    //注意:不要在第一行添加注释或空行，避免$.Import失败！

    //初始化控件的样式定义
    function makeStyle(entity) {
        var me = this;

        //字体
        entity.value.font = {
            size: 0, //index of [0,1,2,3,4,5,6,7,8] 0:默认 8:指定
            fontsize: 0, //${fontsize}px  size=8时有效
            theme: "", //预定义字体色 参见color_choices
            color: "rgb(0,0,0)",//指定字体色。theme优先于color
            bold: false,
            italic: false,
            underline: false,
            strikeout: false,
            family: "",//字体名称，空字符串表示缺省
            //段落
            textIndent: 0,//首行缩进
            lineHeight: 0,//行间距
            wordWrap: false,//换行
            letterSpacing: 0,//字间距
            wordSpacing: 0,//词间距
            align: 0,//文字对齐 [缺省，左对齐，右对齐，居中对齐，两端对齐]
        };

        //背景
        entity.value.background = {
            type: 0,//0:无背景 1:使用纯色 2:使用图片
            url: "",//背景图片
            theme: "",//"":默认 "custom":指定
            color: "rgb(0,0,0)",//指定背景色，为空时使用theme属性
            repeat: 0,//不平铺、平铺、水平重复、垂直重复
            scale: 0,//不缩放 拉伸缩放 等比例缩放
            left: "",
            top: ""
        };

        //对齐
        entity.value.align = {
            vert: 0, //0:默认 1:上对齐 2:居中  3:下对齐
            horz: 0, //0:默认 1:左对齐 2:居中  3:右对齐
        };

        //大小
        entity.value.size = {
            width: {
                type: 0, //0:默认 1:继承 2:占满 3:常用 4:指定 5:最小 6:最大
                unit: 0, //["px","cm","%","em","rem","pt"]
                value: 0
            },
            height: {
                type: 0, //0:默认 1:继承 2:占满 3:常用 4:指定 5:最小 6:最大
                unit: 0, //["px","cm","%","em","rem","pt"]
                value: 0
            }
        };

        //位置
        entity.value.position = {
            //定位
            type: 0, //定位类型：["","inherit","absolute","fixed","relative"]
            unit: 0, //定位单位： type>1 对应：["px","cm","%","em","rem","pt"]
            value: {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
            }, //type>1时有效
            //外边距
            margin: {
                lock: 0, //["四边","上下","左右","单独"],
                size: { //size:["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"],
                    left: 0,//size of left : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    top: 0,//size of top : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    right: 0,//size of right : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    bottom: 0,//size of bottom : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    all: 0,//size of all : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    horz: 0,//size of horz : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    vert: 0//size of vert : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                },
                value: {
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    all: 0,
                    horz: 0,
                    vert: 0
                },
                unit: { //unit:["px","em","rem","pt"]
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    all: 0,
                    horz: 0,
                    vert: 0
                }
            },
            //内边距
            padding: {
                lock: 0, //["四边","上下","左右","单独"],
                size: { //size:["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"],
                    left: 0,//size of left : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    top: 0,//size of top : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    right: 0,//size of right : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    bottom: 0,//size of bottom : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    all: 0,//size of all : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    horz: 0,//size of horz : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                    vert: 0//size of vert : ["缺省","无","极小","迷你","小","普通","大","超大","巨大","指定"]
                },
                value: {
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    all: 0,
                    horz: 0,
                    vert: 0
                },
                unit: { //unit:["px","em","rem","pt"]
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    all: 0,
                    horz: 0,
                    vert: 0
                }
            }
        };

        //边框
        entity.value.border = {
            lock: 0,//["四边","上下","左右","单独"]
            width: {//边框宽度
                left: 0,//["无边框","1px","2px","3px","4px","5px","6px"]
                top: 0,
                right: 0,
                bottom: 0,
                all: 0,
                horz: 0,
                vert: 0
            },
            style: {//边框样式
                left: 3,//["none","dotted","dashed","solid","double"]
                top: 3,
                right: 3,
                bottom: 3,
                all: 3,
                horz: 3,
                vert: 3
            },
            color: {//边框颜色
                left: '#000',//空字符串代表透明，否则代表颜色值
                top: '#000',
                right: '#000',
                bottom: '#000',
                all: '#000',
                horz: '#000',
                vert: '#000'
            }
        };

        //圆角
        entity.value.borderRadius = {
            type: 0,//0:统一 1:单独指定
            value: {
                all: 0,
                tl: 0,//top-left
                tr: 0,//top-right
                bl: 0,//bottom-left
                br: 0//bottom-right
            }
        };

        //阴影
        entity.value.shadow = {
            type: 0,//0:无阴影 1:指定
            x: 3,
            y: 3,
            blur: 3,//模糊半径
            spread: 3,//阴影长度
            color: "#ccc",//阴影颜色
            inset: 0,//是否内部阴影
        };

    }

    //样式生成器
    var styleGen = {
        size_value: ["", "0", "xm", "m", "s", "n", "l", "xl", "xxl", "custom"],
        lock_arr: ["四边", "上下", "左右", "单独"],
        type_arr: ["缺省", "继承", "绝对", "固定", "相对"],
        side_arr: ["all", "vert", "horz", "left", "top", "right", "bottom"],
        side_fix_arr: ["left", "top", "right", "bottom"],//固定定位时使用
        type_value_arr: ["", "inherit", "absolute", "fixed", "relative"],
        unit_arr: ["px", "em", "rem", "pt"],
        size_type_choices: ["自动", "继承", "占满", "常用", "指定", "最小", "最大"],
        size_often_choices: ["400", "300", "200", "180", "160", "120", "100", "80", "60", "45", "30", "20", "15", "10"],
        size_unit_choices: ["px", "cm", "%", "em", "rem", "pt"],
        repeat_arr: ["no-repeat", "repeat", "repeat-x", "repeat-y"],
        border_style_arr: ["none", "dotted", "dashed", "solid"],
        radius_side_arr: ["top-left", "top-right", "bottom-left", "bottom-right"],

        color_choices: ["", "white", "black", "light", "dark",
            "primary", "info", "link", "success",
            "warning", "danger", "black-bis", "black-ter",
            "grey-darker", "grey-dark", "grey", "grey-light",
            "grey-lighter", "white-ter", "white-bis", "custom"],
        color_hints: ["默认",
            "白色", "黑色", "浅色", "深色",
            "主要", "信息", "链接", "成功",
            "警告", "危险", "亮黑", "浅黑",
            "深黑", "深灰", "灰色", "浅灰",
            "亮灰", "浅白", "亮白", "自定义"],
        gen: function (e) {
            var me = this;

            var arr_cls = [];
            var arr_style = [];
            me._genPosition(e, arr_cls, arr_style);
            me._genSize(e, arr_cls, arr_style);
            me._genAlign(e, arr_cls, arr_style);
            me._genText(e, arr_cls, arr_style);
            me._genBackground(e, arr_cls, arr_style);
            me._genBorder(e, arr_cls, arr_style);
            me._genRadius(e, arr_cls, arr_style);
            me._genShadow(e, arr_cls, arr_style);

            var uclass = $.toString(e.value.uclass);
            var ustyle = $.toString(e.value.ustyle);

            return {
                "cls": $.join(arr_cls, " ") + (uclass ? " " + uclass : ""),
                "style": (ustyle ? "" + ustyle + ";" : "") + $.join(arr_style, ";")
            }
        },
        _genPosition: function (entity, arr_cls, arr_style) {
            var me = this;
            var v = entity.value || {};
            v.position = v.position || {};

            v.position.unit = v.position.unit || {};
            v.position.value = v.position.value || {};

            v.position.margin = v.position.margin || {};
            v.position.margin.size = v.position.margin.size || {};
            v.position.margin.value = v.position.margin.value || {};
            v.position.margin.unit = v.position.margin.unit || {};

            v.position.padding = v.position.padding || {};
            v.position.padding.size = v.position.padding.size || {};
            v.position.padding.value = v.position.padding.value || {};
            v.position.padding.unit = v.position.padding.unit || {};

            var side = "";
            var marginsize = 0;
            var paddingsize = 0;
            var m = "";
            var val = 0;
            var unit = 0;

            //type
            var type = $.toInt(v.position.type);
            if (type > 0) {
                arr_style.push("position:" + me.type_value_arr[type]);
            }

            //相对、绝对、固定
            if (type > 1) {
                $.each(me.side_fix_arr, function (side) {
                    var value = $.toInt(v.position.value[side]);
                    if (value) {
                        arr_style.push("" + side + ":" + value + me.unit_arr[$.toInt(v.position.unit[side])])
                    }
                })
            }

            //margin
            var marginlock = $.toInt(v.position.margin.lock);
            if (marginlock < 3) {
                side = me.side_arr[marginlock];
                marginsize = $.toInt(v.position.margin.size[side]);
                if (marginsize < 1) {
                    //do nothing
                } else if (marginsize < 9) {
                    m = "m-";
                    if (marginlock === 1) m = "v" + m;
                    else if (marginlock === 2) m = "h" + m;
                    m += me.size_value[marginsize];
                    arr_cls.push(m);
                } else {
                    //指定大小
                    val = $.toInt(v.position.margin.value[side]);
                    unit = $.toInt(v.position.margin.unit[side]);
                    if (marginlock === 0) arr_style.push("margin:" + val + me.unit_arr[unit]);
                    else if (marginlock === 1) {
                        arr_style.push("margin-top:" + val + me.unit_arr[unit]);
                        arr_style.push("margin-bottom:" + val + me.unit_arr[unit]);
                    } else if (marginlock === 2) {
                        arr_style.push("margin-left:" + val + me.unit_arr[unit]);
                        arr_style.push("margin-right:" + val + me.unit_arr[unit]);
                    }

                }
            } else {
                for (var i = 3; i < me.side_arr.length; i++) {
                    side = me.side_arr[i];
                    marginsize = $.toInt(v.position.margin.size[side]);
                    /*if (marginsize < 1) {
                        //do nothing
                    } else if (marginsize < 9) {
                        m = side.substr(0, 1) + "m-";
                        m += size_value[marginsize];
                        arr_cls.push(m);
                    } else*/
                    {
                        //指定大小
                        val = $.toInt(v.position.margin.value[side]);
                        unit = $.toInt(v.position.margin.unit[side]);
                        arr_style.push("margin-" + side + ":" + val + me.unit_arr[unit]);

                    }
                }
            }

            //padding
            var paddinglock = $.toInt(v.position.padding.lock);
            if (paddinglock < 3) {
                side = me.side_arr[paddinglock];
                paddingsize = $.toInt(v.position.padding.size[side]);
                if (paddingsize < 1) {
                    //do nothing
                } else if (paddingsize < 9) {
                    m = "p-";
                    if (paddinglock === 1) m = "v" + m;
                    else if (paddinglock === 2) m = "h" + m;
                    m += me.size_value[paddingsize];
                    arr_cls.push(m);
                } else {
                    //指定大小
                    val = $.toInt(v.position.padding.value[side]);
                    unit = $.toInt(v.position.padding.unit[side]);
                    if (paddinglock === 0) arr_style.push("padding:" + val + me.unit_arr[unit]);
                    else if (paddinglock === 1) {
                        arr_style.push("padding-top:" + val + me.unit_arr[unit]);
                        arr_style.push("padding-bottom:" + val + me.unit_arr[unit]);
                    } else if (paddinglock === 2) {
                        arr_style.push("padding-left:" + val + me.unit_arr[unit]);
                        arr_style.push("padding-right:" + val + me.unit_arr[unit]);
                    }

                }
            } else {
                for (var j = 3; j < me.side_arr.length; j++) {
                    side = me.side_arr[j];
                    paddingsize = $.toInt(v.position.padding.size[side]);
                    /*if (paddingsize < 1) {
                        // do nothing
                    } else if (paddingsize < 9) {
                        m = side.substr(0, 1) + "p-";
                        m += size_value[paddingsize];
                        arr_cls.push(m);
                    } else*/
                    {
                        //指定大小
                        val = $.toInt(v.position.padding.value[side]);
                        unit = $.toInt(v.position.padding.unit[side]);
                        arr_style.push("padding-" + side + ":" + val + me.unit_arr[unit]);

                    }
                }
            }
        },
        _genSize: function (entity, arr_cls, arr_style) {
            var me = this;

            var v = entity.value || {};
            v.size = v.size || {};

            v.size.width = v.size.width || {};
            v.size.height = v.size.height || {};

            $.each(["width", "height"], function (wh) {
                var type = $.toInt(v.size[wh].type);//0:默认 1:继承 2:占满 3:常用 4:指定 5:最小 6:最大
                if (type > 0) {
                    var unit = $.toInt(v.size[wh].unit);
                    if (type === 1) {//继承
                        arr_style.push("" + wh + ":inherit")
                    } else if (type === 2) {//占满
                        arr_cls.push("" + wh + "-full");
                    } else if (type === 3) {//常用
                        var often = $.toInt(v.size[wh].often);
                        arr_cls.push("" + wh + "-" + me.size_often_choices[often]);
                    } else if (type === 4) {//指定

                        arr_style.push("" + wh + ":" + v.size[wh].value + me.size_unit_choices[unit])
                    } else if (type === 5) {//最小
                        arr_style.push("min-" + wh + ":" + v.size[wh].value + me.size_unit_choices[unit])
                    } else if (type === 6) {//最大
                        arr_style.push("max-" + wh + ":" + v.size[wh].value + me.size_unit_choices[unit])
                    }

                }
            });
        },
        _genAlign: function (entity, arr_cls, arr_style) {
            var me = this;

            var v = entity.value || {};
            v.align = v.align || {};

            var vert = $.toInt(v.align.vert);
            if (vert) {
                if (vert === 1) {

                } else if (vert === 2) {
                    arr_cls.push("flex-middle")
                } else if (vert === 3) {
                    arr_cls.push("flex-bottom")
                }
            }

            var horz = $.toInt(v.align.horz);
            if (horz) {
                if (horz === 1) {

                } else if (horz === 2) {
                    arr_cls.push("flex-center")
                } else if (horz === 3) {
                    arr_cls.push("flex-right")
                }
            }

        },
        _genText: function (entity, arr_cls, arr_style) {
            var me = this;

            var v = entity.value || {};
            v.font = v.font || {};

            //fontColor
            var theme = v.font.theme;
            if (theme) {
                if (theme === 'custom') arr_style.push("color:" + v.font.color);
                else arr_cls.push("has-text-" + theme);
            }

            //fontSize
            var sizeIndex = $.toInt(v.font.size);
            if (sizeIndex) {
                if (sizeIndex >= 8) {
                    var fontsize = $.toInt(v.font.fontsize);
                    if (fontsize < 8) fontsize = 8;
                    arr_style.push("font-size:" + fontsize + "px");
                } else {
                    arr_cls.push("is-size-" + sizeIndex);
                }
            }

            //fontFamily
            var family = v.font.family;
            if (family) arr_style.push("font-family:'" + family + "'");

            //fontStyle
            var bold = $.toInt(v.font.bold);
            if (bold) arr_cls.push("has-text-weight-bold");

            var italic = $.toInt(v.font.italic);
            if (italic) arr_cls.push("is-italic");

            var underline = $.toInt(v.font.underline);
            if (underline) arr_style.push("text-decoration:underline");

            var strikeout = $.toInt(v.font.strikeout);
            if (strikeout) arr_style.push("text-decoration:line-through");
        },
        _genBackground: function (entity, arr_cls, arr_style) {
            var me = this;

            var v = entity.value || {};
            v.background = v.background || {};

            var type = $.toInt(v.background.type);
            if (type) {
                if (type === 1) {
                    //背景颜色
                    var theme = v.background.theme;
                    if (theme) {
                        if (theme === 'custom') arr_style.push("background-color:" + v.background.color);
                        else
                            arr_cls.push("has-background-" + theme);
                    }

                } else if (type === 2) {
                    //背景图片
                    if (v.background.url) {

                        arr_style.push("background-image:url(" + v.background.url + ")");

                        var repeat = me.repeat_arr[$.toInt(v.background.repeat)];
                        arr_style.push("background-repeat:" + repeat);

                        var scale = $.toInt(v.background.scale);
                        if (scale) {
                            if (scale === 1) arr_style.push("background-size:100% 100%");
                            else if (scale === 2) arr_style.push("background-size:cover");
                        }

                        var left = v.background.left;
                        var top = v.background.top;
                        if (!scale && !v.background.repeat) {
                            if (!left) left = "0";
                            if (!top) top = "0";
                            arr_style.push("background-position:" + left + " " + top);
                        }
                    }

                }
            }
        },
        _genBorder: function (entity, arr_cls, arr_style) {
            var me = this;
            var v = entity.value || {};
            v.border = v.border || {};
            var lock = $.toInt(v.border.lock);
            $.each(["all", "vert", "horz", "left", "right", "top", "bottom"], function (side) {
                var width = $.toInt(v.border.width[side]);
                var style = $.toInt(v.border.style[side]);
                var color = v.border.color[side];
                if (width && style && color) {
                    if (lock === 0) {
                        if (side === 'all') {
                            arr_style.push("border:" + width + "px " + me.border_style_arr[style] + " " + color);
                        }
                    } else if (lock === 1) {
                        if (side === 'vert') {
                            arr_style.push("border-top:" + width + "px " + me.border_style_arr[style] + " " + color);
                            arr_style.push("border-bottom:" + width + "px " + me.border_style_arr[style] + " " + color);
                        }
                    } else if (lock === 2) {
                        if (side === 'horz') {
                            arr_style.push("border-left:" + width + "px " + me.border_style_arr[style] + " " + color);
                            arr_style.push("border-right:" + width + "px " + me.border_style_arr[style] + " " + color);
                        }
                    } else {
                        if (side === 'left' || side === 'right' || side === 'top' || side === 'bottom') {
                            arr_style.push("border-" + side + ":" + width + "px " + me.border_style_arr[style] + " " + color);
                        }
                    }
                }
            });
        },
        _genRadius: function (entity, arr_cls, arr_style) {
            var me = this;
            var v = entity.value || {};
            v.borderRadius = v.borderRadius || {};
            var type = $.toInt(v.borderRadius.type);
            if (type) {
                $.each(["tl", "tr", "bl", "br"], function (side, index) {
                    var val = $.toInt(v.borderRadius.value[side]);
                    if (val) arr_style.push("border-" + me.radius_side_arr[index] + "-radius:" + val + "px");
                })
            } else {
                var val = $.toInt(v.borderRadius.value.all);
                if (val) arr_style.push("border-radius:" + val + "px");
            }

        },
        _genShadow: function (entity, arr_cls, arr_style) {
            var me = this;
            var v = entity.value || {};
            v.shadow = v.shadow || {};
            if ($.toInt(v.shadow.type)) {
                var inset = $.toInt(v.shadow.inset);
                var x = $.toInt(v.shadow.x);
                var y = $.toInt(v.shadow.y);
                var blur = $.toInt(v.shadow.blur);
                var spread = $.toInt(v.shadow.spread);
                var color = $.toString(v.shadow.color);
                if (x || y)
                    arr_style.push("box-shadow:" + x + "px " + y + "px " + blur + "px " + spread + "px " + color + (inset ? " inset" : ""));
            }
        }
    };

    //undo & redo
    var undoMaker = {
        create: function () {
            return {
                _deep: 100,
                _afterUndo: null,
                _afterRedo: null,
                _stUndo: [],
                _stRedo: [],
                /*
                action:{
                    redo:function(){},
                    undo:function(){}
                * */
                add: function (action) {
                    this._stUndo.push(action);
                    if (this._stUndo.length > this._deep) {
                        this._stUndo.shift();
                    }
                    this.stRedo = [];
                },
                canUndo: function () {
                    return this._stUndo.length > 0;
                },
                canRedo: function () {
                    return this._stRedo.length > 0;
                },
                undo: function () {
                    var action = this._stUndo.pop();
                    action.undo();
                    try {
                        if ($.isFunction(this._afterUndo)) this._afterUndo();
                    } catch (e) {
                    }
                    this._stRedo.push(action);
                },
                redo: function () {
                    var action = this._stRedo.pop();
                    action.redo();
                    try {
                        if ($.isFunction(this._afterRedo)) this._afterRedo();
                    } catch (e) {
                    }
                    this._stUndo.push(action);
                },
                setConfig: function (cfg) {
                    if ("deep" in cfg) this._deep = $.toInt(cfg.deep);
                    if ($.isFunction(cfg.afterUndo)) this._afterUndo = cfg.afterUndo;
                    if ($.isFunction(cfg.afterRedo)) this._afterRedo = cfg.afterRedo;
                }
            }
        }
    };

    var undoGroupMaker = {
        create: function () {
            return {
                actions: [],//0: prop 1:create 2:delete 3:movepos 4:movein 5:moveout
                reset: function () {
                    this.actions = []
                },
                changeProp: function (entity, path, value) {
                    this.actions.push({
                        type: 0, //propChange
                        entity: entity,
                        path: path,
                        oldValue: $.getValue(entity.value, path),
                        newValue: value
                    });
                },
                createEntity: function (entity, parentChildren, index) {
                    this.actions.push({
                        type: 1,
                        entity: entity,
                        parentChildren: parentChildren,
                        index: index
                    })
                },
                removeEntity: function (entity, parentChildren, index) {
                    this.actions.push({
                        type: 2,
                        entity: entity,
                        parentChildren: parentChildren,
                        index: index
                    })
                },
                moveEntity: function (entity, oldParentChildren, oldPos, newParentChildren, newPos) {
                    this.actions.push({
                        type: 5,
                        entity: entity,
                        oldParentChildren: oldParentChildren,
                        oldPos: oldPos,
                        newParentChildren: newParentChildren,
                        newPos: newPos
                    })
                },
                flush: function (undo) {
                    var me = this;
                    if (me.actions.length > 0) {
                        undo.add({
                            actions: me.actions,
                            undo: function () {
                                var oThis = this;
                                $.each(oThis.actions, function (action) {
                                    switch (action.type) {
                                        case 0://prop
                                            $.setValue(action.entity.value, action.path, action.oldValue);
                                            break;
                                        case 1://create
                                            action.parentChildren.splice(action.index, 1);
                                            break;
                                        case 2://delete
                                            action.parentChildren.splice(action.index, 0,action.entity);
                                            break;
                                        case 3://move
                                            action.parentChildren.splice(action.newPos, 1);
                                            action.parentChildren.splice(action.oldPos,0,action.entity);
                                            break;
                                        case 4://movein
                                            action.oldParentChildren.splice(action.newPos,1);
                                            action.newParentChildren.splice(action.oldPos,0,action.entity);
                                            //action.
                                            break;
                                        case 5://moveout
                                            action.oldParentChildren.splice(action.newPos,1);
                                            action.newParentChildren.splice(action.oldPos,0,action.entity);
                                            break;
                                    }
                                })
                            },
                            redo: function () {
                                var oThis = this;
                                $.each(oThis.actions, function (action) {
                                    switch (action.type) {
                                        case 0://prop
                                            $.setValue(action.entity.value, action.path, action.newValue);
                                            break;
                                        case 1://create
                                            action.parentChildren.splice(action.index, 0, action.entity);
                                            break;
                                        case 2://delete
                                            action.parentChildren.splice(action.index, 1);
                                            break;
                                        case 3://move
                                            action.parentChildren.splice(action.oldPos, 1);
                                            action.parentChildren.splice(action.newPos,0,action.entity);
                                            break;
                                        case 4://movein
                                            action.oldParentChildren.splice(action.oldPos,1);
                                            action.newParentChildren.splice(action.newPos,0,action.entity);
                                            //action.
                                            break;
                                        case 5://moveout
                                            action.oldParentChildren.splice(action.oldPos,1);
                                            action.newParentChildren.splice(action.newPos,0,action.entity);
                                            break;
                                    }
                                })
                            }
                        })
                    }
                    this.actions = [];
                }
            }
        }
    };

    return {
        //创建一个richedit
        create: function (cfg, editor, owner) {
            return {
                /*
                编辑器的配置
                cfg:{
                    fnSave:function(owner,fnOK,fnFail){},//保存编辑的内容
                    fnCreate:function(key){},//创建扩展的控件实例
                    fnIsContainer:function(entity){},//判断扩展控件是否为容器类控件
                    fnUploadFile:function(file,fnOK,fnFail){},//上传文件 fnOK(path)  fnFail(err)

                    autoSaveInterval:0,//自动保存的间隔时间，单位为秒，为0表示不支持自动保存
                }
                * */
                cfg: cfg,//编辑器配置

                /*
                编辑器操作接口
                    getUrl:function(){},//得到editor视图对应的url
                    refresh:function(){},//更新界面(只更新操作区)
                    refreshAll:function(){},//更新所有界面
                    setFullScreen:function(bFull){},//切换全屏显示
                    getFullScreen:function(){},//判断是否在全屏状态
                * */
                editor: editor,//编辑器操作接口

                /*
                控件操作接口，由控件实例自动维护，如：
                controlActions[ entity.id ] = editAction;
                控件操作接口至少要提供：
                    refresh:function(bRecure){},//更新实例显示
                * */
                controlActions: {},

                /*
                * 画布操作接口
                * 至少提供函数：
                * refresh:function(){} //更新界面
                * */
                canvas: null,

                /*
                * 工具栏操作接口
                * 至少提供函数：
                * refresh:function(){} //更新界面
                * */
                toolbar: null,

                /*
                * 控件实例对应的dom对象，由控件实例自动维护
                * 如：doms[ entity.id ] = entityDom
                * */
                doms: {},

                /*
                undo & redo
                * */
                undoMgr: undoMaker.create(),
                undoGroup: undoGroupMaker.create(),

                //所有当前选中的实例
                selected: [],

                //当前焦点实例
                focus: null,

                //修改标志
                isModified : false,

                /*
                虚拟的根控件实例
                示例：
                owner:{
                    id:0,
                    name:"",
                    value:{},
                    children:[],//所有的实例[{id:0,name:"",value:{},children:[]},...]
                },
                * */
                owner: owner,

                //id生成器
                maxid: 0,

                //======= 基本的实例操作(entity类扩展) [begin] ============================
                getID: function (entity) {
                    if( !entity ) return -1;
                    return entity.id;
                },
                setID: function (entity, id) {
                    entity.id = id;
                },

                getType: function (entity) {
                    return entity.name;
                },
                setType: function (entity, type) {
                    entity.name = type;
                },

                getCls: function (entity) {
                    return entity.value.cls;
                },
                setCls: function (entity, cls) {
                    entity.value.cls = cls;
                },

                getStyle: function (entity) {
                    return entity.value.style;
                },
                setStyle: function (entity, style) {
                    entity.value.style = style;
                },

                getChildren: function (entity) {
                    return entity.children || [];
                },
                isContainer: function (entity) {
                    var me = this;
                    var ty = me.getType(entity);
                    if( entity === me.owner ) return true;
                    //""代表的是根元素
                    if ($.indexOfArray(ty, ["", "container","cols","col"], function (a, b) {
                        return a === b;
                    }) >= 0) return true;
                    var f = me.cfg.fnIsContainer;
                    if ($.isFunction(f)) return me.cfg.fnIsContainer(entity);
                    return false;
                },

                getProp: function (entity, path) {
                    var me = this;
                    return $.getValue(entity.value, path);
                },
                setProp: function (entity, path, val) {
                    var me = this;
                    $.setValue(entity.value, path, val);
                    me.refreshEntity(entity);
                    me.undoGroup.changeProp(entity,path,val);

                    var ret = styleGen.gen(entity);
                    entity.value.cls = ret.cls;
                    entity.value.style = ret.style;

                    me.isModified = true;
                },

                refreshEntity:function(entity){
                    var me = this;
                    var act = me.getAction(entity);
                    if( act ) act.refresh(true);
                },
                isFocus:function(entity){
                    var me = this;
                    return me.focus && entity && (entity === me.focus || me.getID(entity) ===me.getID(me.focus));
                },

                //是否可即地编辑的控件
                isInplaceEdit:function(entity){
                    var me = this;
                    var type = "";
                    if( entity ) type = me.getType(entity);
                    return type==="textbox" || type==="header" || type==="text";
                },

                //是否可直接对控件内文本设置格式
                isCharFormatable:function(entity){
                    var me = this;
                    var type = "";
                    if( entity ) type = me.getType(entity);
                    return type==="textbox" ;
                },

                //得到即地编辑器选中文本字符的个数
                getSelectCharCount:function(entity){
                    if( !entity ) return 0;
                    var selection = window.getSelection();
                    var range = selection.getRangeAt(0);
                    return range.endOffset - range.startOffset;
                },

                //======= 基本的实例操作(entity类扩展) [end] ============================

                //========== 查找与关系判断 [begin] =================================
                getIndex: function (entity, parent) {
                    var me = this;
                    if (!parent) parent = me.owner;
                    return $.indexOfArray(entity, parent.children, function (a, b) {
                        return me.getID(a) === me.getID(b)
                    });
                },

                //根据id查找实例
                // from可选 不传时代表从owner开始找起
                find: function (id, from) {
                    var me = this;
                    var found = null;
                    if (!from) from = me.owner;
                    var children = me.getChildren(from);

                    var index = $.indexOfArray(id, children, function (a, b) {
                        return a === me.getID(b)
                    });
                    if (index >= 0) found = children[index];
                    else {
                        $.each(children, function (a) {
                            found = me.find(id, a);
                            if (found) return true;
                        })
                    }
                    return found;
                },

                //查找父实例
                // from可选 不传时代表从owner开始找起
                getParent: function (entity, from) {
                    var me = this;
                    var found = null;
                    if (!from) from = me.owner;
                    var index = me.getIndex(entity, from);
                    if (index >= 0) found = from;
                    else {
                        $.each(me.getChildren(from), function (e) {
                            found = me.getParent(entity, e);
                            if (found) return true;
                        });
                    }
                    return found;
                },
                //判断是否为它的后代
                descendantOf: function (descendant, parent) {
                    var me = this;
                    var children = me.getChildren(parent);
                    var count = children.length;
                    var id = me.getID(descendant);
                    for (var i = 0; i < count; i++) {
                        var e = children[i];
                        if (me.descendantOf(descendant, e)) return true;
                        if (id === me.getID(e)) return true;
                    }
                    return false;
                },
                //========== 查找与关系判断 [end] =================================

                //========== 实例的生命周期 [begin] =================================
                createID: function () {
                    return ++this.maxid;
                },
                //根据类型创建新的控件实例
                createEntity: function (type,parent,index) {
                    var me = this;
                    var entity = {};
                    me.setID(entity, me.createID());
                    me.setType(entity, type);

                    entity.value = {cls: "", style: ""};
                    entity.children = [];
                    makeStyle(entity);
                    switch (type) {
                        case "text":
                            entity.value.text = "文本";
                            break;
                        case "image":
                            entity.value.url = $.getConfig().pageroot + $.GetRelatePath("images/empty-image.png", me.editor.getUrl());
                            entity.value.scale=true;
                            break;
                        case "link":
                            entity.value.text="链接";
                            entity.value.title="链接";
                            entity.value.url="https://www.baijienet.com";
                            break;
                        case "container":
                            //容器的缺省样式
                            entity.value.border.lock = 0;
                            entity.value.border.width.all = 1;
                            entity.value.border.style.all=1;
                            entity.value.border.color.all="#e0e0e0";
                            //entity.value.size.height.type=4;
                            //entity.value.size.height.value=80;
                            entity.value.position.margin.lock=1;//vert
                            entity.value.position.margin.size.vert=9;
                            entity.value.position.margin.value.vert=10;//marginV=10px
                            entity.value.position.padding.lock=0;//all
                            entity.value.position.padding.size.all=9;
                            entity.value.position.padding.value.all=10;//paddingV=10px

                            break;
                        case "cols":
                            //分栏的缺省样式
                            entity.value.border.lock = 0;
                            entity.value.border.width.all = 1;
                            entity.value.border.style.all=1;
                            entity.value.border.color.all="#e0e0e0";
                            entity.value.size.height.type=4;
                            entity.value.size.height.value=80;
                            //
                            entity.value.ismultiline=false;
                            entity.value.isgapless=false;
                            break;
                        case "col":
                            entity.value.isnarrow=true;
                            entity.value.ratio=1;//占比  1-12:占比  0:自动填充(isnarrow=true：自动大小)
                            entity.value.background.color="#e3e3e3";
                            entity.value.background.type=1;
                            entity.value.background.theme="custom";
                            break;
                        case "video":
                            entity.value.border.lock = 0;
                            entity.value.border.width.all = 1;
                            entity.value.border.style.all=1;
                            entity.value.border.color.all="#e0e0e0";

                            entity.value.size.width.type=4;
                            entity.value.size.width.value=320;
                            entity.value.size.height.type=4;
                            entity.value.size.height.value=200;
                            entity.value.url="";
                            break;
                        case "textbox":
                            //entity.value.border.lock = 0;
                            //entity.value.border.width.all = 1;
                            //entity.value.border.style.all=1;
                            //entity.value.border.color.all="#e0e0e0";
                            //entity.value.size.height.type=4;
                            //entity.value.size.height.value=80;
                            //entity.value.text="请输入文字";
                            entity.value.position.margin.lock=1;//vert
                            entity.value.position.margin.size.vert=9;
                            entity.value.position.margin.value.vert=10;//marginV=10px
                            entity.value.position.padding.lock=0;//all
                            entity.value.position.padding.size.all=9;
                            entity.value.position.padding.value.all=10;//paddingV=10px
                            break;
                        case "header":

                            //entity.value.size.height.type=4;
                            //entity.value.size.height.value=80;
                            //entity.value.text="请输入文字";
                            entity.value.text="";
                            entity.value.font.size=5;
                            entity.value.font.bold=true;
                            entity.value.font.theme="warning";
                            entity.value.position.margin.lock=1;//vert
                            entity.value.position.margin.size.vert=9;
                            entity.value.position.margin.value.vert=10;//marginV=10px

                            break;
                    }

                    var ret = styleGen.gen(entity);
                    entity.value.cls = ret.cls;
                    entity.value.style = ret.style;

                    if( parent ){
                        var children = me.getChildren(parent);
                        if( index ){
                            index = $.toInt(index);
                            if( index < 0 || index > children.length-1 ) {
                                me.undoGroup.createEntity(entity,children,children.length);
                                children.push(entity);
                            }
                            else {
                                me.undoGroup.createEntity(entity,index);
                                children.splice(index,0,entity);
                            }
                        }else{
                            me.undoGroup.createEntity(entity,children,children.length);
                            children.push(entity);
                        }
                        me.refreshEntity(parent);
                    }
                    me.isModified = true;
                    if( me.toolbar ) me.toolbar.refresh();
                    return entity;
                },
                //删除实例
                removeEntity: function (entity) {
                    var me = this;
                    if( !entity ) return;
                    var id = me.getID(entity);

                    me.unselect(entity);
                    var parent = me.getParent(entity);
                    var index = me.getIndex(entity, parent);
                    var children = me.getChildren(parent);
                    children.splice(index, 1);
                    if( id in me.controlActions ) delete me.controlActions[id];
                    me.undoGroup.removeEntity(entity,children,index);

                    me.isModified = true;
                    if( me.toolbar ) me.toolbar.refresh();
                },
                //删除所有选中的实例
                removeAllSelected:function(){
                    var me = this;
                    $.each(me.selected,function (e) {
                        me.removeEntity(e)
                    });
                    me.selected=[];
                    me.focus=null;
                    me.isModified = true;
                },
                //格式化实例
                format: function (entity) {
                    var me = this;
                    me.setID(entity, me.createID());
                    $.each(me.getChildren(entity), function (e) {
                        me.format(e);
                    });
                },
                //========== 实例的生命周期 [end] =================================

                //========== 实例的选择 [begin] =================================
                //选中
                select: function (entity) {
                    this.focus = entity;
                    this.selected.push(entity);
                },
                //取消选中
                unselect: function (entity) {
                    var me = this;
                    var isFocus = me.isFocus(entity);

                    var index = $.indexOfArray(entity, me.selected, function (a, b) {
                        return me.getID(a) === me.getID(b)
                    });
                    if (index >= 0) {
                        me.selected.splice(index, 1);
                        if( isFocus ){
                            if (me.selected.length > 0 ) me.focus = me.selected[0];
                        }
                    }
                },
                //清空选择
                clearSelect: function () {
                    this.focus = null;
                    this.selected = [];
                },
                //选中下一个
                selectNext: function (entity) {
                    var me = this;
                    var parent = me.getParent(entity);
                    var index = me.getIndex(entity, parent);
                    var children = me.getChildren(parent);
                    var next = (index >= children.length - 1 ? children[0] : children[index + 1]);
                    me.selected.push(next);
                    me.focus = next;
                },
                //选中上一个
                selectPrev: function (entity) {
                    var me = this;
                    var parent = me.getParent(entity);
                    var index = me.getIndex(entity, parent);
                    var children = me.getChildren(parent);
                    var prev = (index > 0 ? children[index - 1] : children[children.length - 1]);
                    me.selected.push(prev);
                    me.focus = prev;
                },
                //选择上一级
                selectUp: function (entity) {
                    var me = this;
                    var parent = me.getParent(entity);
                    me.selected.push(parent);
                    me.focus = parent;
                },
                //选择下一级
                selectDown: function (entity) {
                    var me = this;
                    if (me.isContainer(entity)) {
                        var children = me.getChildren(entity);
                        if (children.length > 0) {
                            me.selected.push(children[0]);
                            me.focus = children[0];
                        }
                    }
                },

                //根据落点计算元素
                hitTest: function (x, y, from) {
                    var me = this;
                    if (!from) from = me.owner;

                    var dom = me.getDom(from);
                    if (!dom) return null;
                    var oNode = dom.node();

                    var children = me.getChildren(from);

                    //$.log("== hitTest from:"+from.id);
                    var r = oNode.getBoundingClientRect();

                    var left = r.left;
                    var top = r.top;
                    var width = r.width;
                    var height = r.height;
                    //$.log("hitTesting   x="+x+" y="+y+" left="+left+" top="+top+" width="+width+" height="+height);

                    if (from === me.owner || (x >= left && x <= left + width && y >= top && y <= top + height)) {
                        //落在from元素内，继续判断子元素
                        var len = children.length;
                        for (var i = 0; i < len; i++) {
                            var entity = children[i];
                            var p = me.hitTest(x, y, entity);
                            if (p) return p;
                        }
                        return from === me.owner ? null : from;
                    } else return null;
                },
                //测试离哪个元素最近
                testPos: function (container, x, y) {
                    var me = this;
                    var ret = {entity: null, isBefore: false};
                    var posInfos = [];
                    var children = me.getChildren(container);
                    $.each(children, function (entity) {
                        var oNode = me.getDom(me.getID(entity)).node();

                        //$.log("== hitTest from:"+from.id);
                        var r = oNode.getBoundingClientRect();
                        var left = r.left;
                        var top = r.top;
                        var width = r.width;
                        var height = r.height;

                        var obj = {
                            entity: entity,
                            delta: 9999999,
                            side: ""
                        };
                        if (x < left) {
                            if (left - x < obj.delta) {
                                obj.delta = left - x;
                                obj.side = "left";
                            }
                        }
                        if (y < top) {
                            if (top - y < obj.delta) {
                                obj.delta = top - y;
                                obj.side = "top";
                            }
                        }
                        if (x > left + width) {
                            if (x - (left + width) < obj.delta) {
                                obj.delta = x - (left + width);
                                obj.side = "right";
                            }
                        }
                        if (y > top + height) {
                            if (y - (top + height) < obj.delta) {
                                obj.delta = y - (top + height);
                                obj.side = "bottom";
                            }
                        }
                        posInfos.push(obj);
                    });
                    //找出值最小的那一个
                    var delta = 999999;
                    var found = -1;
                    $.each(posInfos, function (pos, index) {
                        if (pos.delta < delta) {
                            found = index;
                            delta = pos.delta;
                        }
                    });
                    return posInfos[found];
                },
                //========== 实例的选择 [end] =================================

                //============ 实例的移动 [begin] ======================
                moveUp: function (entity) {
                    var me = this;
                    var parent = me.getParent(entity);
                    var index = me.getIndex(entity, parent);
                    var children = me.getChildren(parent);
                    children.splice(index, 1);
                    var newIndex = index-1;

                    if (newIndex < 0) {
                        newIndex = children.length;
                        children.push(entity);
                    }
                    else children.splice(newIndex, 0, entity);
                    me.undoGroup.moveEntity(entity,children,index,children,newIndex);
                    me.isModified = true;
                },
                moveDown: function (entity) {
                    var me = this;

                    var parent = me.getParent(entity);
                    var index = me.getIndex(entity, parent);
                    var children = me.getChildren(parent);
                    if (index >= children.length - 1) {
                        children.splice(index, 1);
                        children.splice(0, 0, entity);
                        me.undoGroup.moveEntity(entity,children,index,children,0);
                    } else {
                        children.splice(index, 1);
                        if (index + 1 > children.length - 1) {
                            me.undoGroup.moveEntity(entity,children,index,children,children.length);
                            children.push(entity);
                        }
                        else {
                            me.undoGroup.moveEntity(entity,children,index,children,index+1);
                            children.splice(index + 1, 0, entity);
                        }
                    }
                    me.isModified = true;
                },
                moveIn: function (entity) {
                    var me = this;

                    var parent = me.getParent(entity);
                    if (parent) {
                        var index = me.getIndex(entity, parent);
                        var children = me.getChildren(parent);
                        var next = null;
                        if (index < children.length - 1) next = children[index + 1];
                        if (!next || !me.isContainer(next)) {
                            if (index > 0) next = children[index - 1];
                        }
                        if (next && me.isContainer(next)) {
                            children.splice(index, 1);
                            var nextChildren = me.getChildren(next);
                            nextChildren.push(entity);
                            me.undoGroup.moveEntity(entity,children,index,nextChildren,nextChildren.length-1);
                        }
                    }
                    me.isModified = true;
                },
                moveOut: function (entity) {
                    var me = this;
                    var parent = me.getParent(me.getID(entity));
                    if (parent && parent !== me.owner) {
                        var index = me.getIndex(entity, parent);

                        var parent2 = me.getParent(parent);
                        if (parent2) {
                            var index2 = me.getIndex(parent, parent2);

                            var children = me.getChildren(parent);
                            var children2 = me.getChildren(parent2);
                            children.splice(index, 1);
                            children2.splice(index2, 0, entity);

                            me.undoGroup.moveEntity(entity,children,index,children2,index2);
                        }
                    }
                    me.isModified = true;
                },
                moveTo: function (entity, target, isBefore) {
                    var me = this;
                    if (!me.descendantOf(entity, target)) {
                        var targetParent = me.getParent(target);
                        var targetIndex = me.getIndex(target, targetParent);
                        var tpChildren = me.getChildren(targetParent);

                        var parent = me.getParent(entity);
                        var index = me.getIndex(entity, parent);
                        me.getChildren(parent).splice(index, 1);
                        if (isBefore) {
                            tpChildren.splice(targetIndex, 0, entity);
                            me.undoGroup.moveEntity(entity,me.getChildren(parent),index,tpChildren,targetIndex);
                        } else {
                            if (targetIndex < tpChildren.length - 1) {
                                me.undoGroup.moveEntity(entity,me.getChildren(parent),index,tpChildren,targetIndex+1);
                                tpChildren.splice(targetIndex + 1, 0, entity);
                            } else {
                                me.undoGroup.moveEntity(entity,me.getChildren(parent),index,tpChildren,tpChildren.length);
                                tpChildren.push(entity);
                            }
                        }
                    }
                    me.isModified = true;
                },
                //============ 实例的移动 [end] ======================

                //============= 接口安装 [begin] =======================
                setDom: function (entity, dom) {
                    this.doms[this.getID(entity)] = dom;
                },
                getDom: function (entity) {
                    var id = this.getID(entity);
                    if (id in this.doms) return this.doms[id];
                    return null;
                },
                removeDom:function(entity){
                    var me = this;
                    var id = me.getID(entity);
                    if( id in me.doms ) delete me.doms[id];
                },

                setAction: function (entity, action) {
                    this.controlActions[this.getID(entity)] = action;
                },
                getAction: function (entity) {
                    var id = this.getID(entity);
                    if (id in this.controlActions) return this.controlActions[id];
                    return null;
                },
                removeAction:function(entity){
                    var id = this.getID(entity);
                    if (id in this.controlActions) delete this.controlActions[id];
                },

                setCanvas: function (canvas) {
                    this.canvas = canvas;
                },
                getCanvas: function () {
                    return this.canvas;
                },

                setToolbar: function (tb) {
                    this.toolbar = tb;
                },
                getToolbar: function () {
                    return this.toolbar;
                },
                //============= 接口安装 [end] =======================

                //============ undo & redo [begin] =================
                canUndo:function () {
                    return this.undoMgr.canUndo();
                },
                canRedo:function () {
                    return this.undoMgr.canRedo();
                },
                undo:function () {
                    this.undoMgr.undo();
                },
                redo:function () {
                    this.undoMgr.redo();
                },
                //============ undo & redo [end] =================

                //================ UI交互操作接口(通用style相关) [begin] ==================
                uiChangeProp:function (path,newVal) {
                    var me = this;
                    if( me.focus ){
                        $.each(me.selected,function (entity) {
                            me.setProp(entity,path,newVal);
                            me.refreshEntity(entity);
                        });
                        me.isModified = true;
                        if( me.toolbar ) me.toolbar.refresh();
                    }
                },
                uiMoveUp:function(){
                    var me = this;
                    $.each(me.selected,function (entity) {
                        me.moveUp(entity)
                    });
                    me.isModified = true;
                    if( me.toolbar ) me.toolbar.refresh();
                },
                uiMoveDown:function(){
                    var me = this;
                    $.each(me.selected,function (entity) {
                        me.moveDown(entity)
                    });
                    me.isModified = true;
                    if( me.toolbar ) me.toolbar.refresh();
                },
                uiMoveIn:function(){
                    var me = this;
                    $.each(me.selected,function (entity) {
                        me.moveIn(entity)
                    });
                    me.isModified = true;
                    if( me.toolbar ) me.toolbar.refresh();
                },
                uiMoveOut:function(){
                    var me = this;
                    $.each(me.selected,function (entity) {
                        me.moveOut(entity)
                    });
                    me.isModified = true;
                    if( me.toolbar ) me.toolbar.refresh();
                },
                uiMoveTo:function(){
                    var me = this;
                    me.isModified = true;
                    if( me.toolbar ) me.toolbar.refresh();
                },
                //================ UI交互操作接口(通用style相关) [end] ==================

                //================ richedit 基本操作 [begin] =============================
                getModified:function () {
                    return this.isModified;
                },
                setModified:function (b) {
                    this.isModified = b;
                },

                getFocus:function () {
                    return this.focus;
                },
                getStyleGen:function () {
                    return styleGen;
                }
                //================ richedit 基本操作 [end] =============================

            }
        }
    }
})();
//# sourceURL=ebfui-editor-richedit.dev.js