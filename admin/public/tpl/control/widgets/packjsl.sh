#!/bin/bash

function getdir(){
    cd $1
    for element in `ls $1`
    do  
        dir_or_file=$1"/"$element
        
        if [ -d "$dir_or_file" ]
        then
            getdir $dir_or_file
        else
            if [ ${dir_or_file##*dev.} == "js" ]
            then
                compress $dir_or_file
            fi
            
        fi
    done
}
function compress(){
	a=$1
	ov=${a%.dev*}.min.js
	java -jar "/diskd/baijiesvn/yuicompressor-2.4.8.jar" --type js --charset utf-8 "$1" -o "$ov"
}
root_dir=`dirname $0`
getdir $root_dir
