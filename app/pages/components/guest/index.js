var $ = getApp().globalData.eui;
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		hint:{
			type:String,
			value:"您还没有登录"
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		visible:true
	},
	options: {
		addGlobalClass: true,
	},
	/**
	 * 组件的方法列表
	 */
	methods: {
		onLogin:function(e){
			var fn = $.getConfig("onLogin");
			fn();
		},
		onHide:function (e) {
			var me = this;
			me.setData({
				visible:false
			})
		}
	}
});
