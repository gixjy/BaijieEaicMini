// pages/components/funcentry/index.js
var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        catname: {
            type: String,
            value: ""
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        wide: 3,
        entries: []
    },
    options: {
        addGlobalClass: true,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onEntry: function (e) {
            var me = this;
            var index = $.toInt(e.currentTarget.dataset.index);
            var entry = me.data.entries[index];
            var type = $.toInt(entry.type);
            if( type === 2 ){
                $.go("/pages/product/catlist?catid="+entry.targetid);
            }else{
                $.go(entry.url);
            }

        }
    },
    lifetimes: {
        // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
        attached: function () {
            var me = this;
            var count = me.data.entries.length;
            var wide = 3;
            /*if (count >= 4 && (count % 4 === 0 || count % 4 >= 2)) wide = 4;
            else if (count <= 2) wide = 6;
            else wide=3;*/

            me.setData({
                "wide": wide
            });
            $.api("mall.FuncEntry.LoadAll", {
                    "catname": me.data.catname
                })
                .then(function (value) {
                    $.each(value, function (entry) {
                        var icon = entry.icon;
                        if (icon.indexOf("/") === 0) {
                            entry.icon = $.getConfig("server_url") + icon;
                        }
                    });
                    me.setData({
                        "entries": value
                    })
                })
        },
        detached: function () {
            var me = this;
            if (me.intval) {
                clearInterval(me.intval);
            }
        },
    },
})