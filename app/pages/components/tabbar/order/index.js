var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        "meta": {
            unpay: {
                "cond": {
                    "pageindex": 1,
                    "pagesize": 10,
                    statuscat: 1
                },
                totalpage: 1,
                "datas": []
            },
            unrec: {
                "cond": {
                    "pageindex": 1,
                    "pagesize": 10,
                    statuscat: 2
                },
                totalpage: 1,
                "datas": []
            },
            inpin: {
                "cond": {
                    "pageindex": 1,
                    "pagesize": 10,
                    statuscat: 3
                },
                totalpage: 1,
                "datas": []
            },
            finish: {
                "cond": {
                    "pageindex": 1,
                    "pagesize": 10,
                    statuscat: 4
                },
                totalpage: 1,
                "datas": []
            },
            all: {
                "cond": {
                    "pageindex": 1,
                    "pagesize": 10,
                    statuscat: 0
                },
                totalpage: 1,
                "datas": []
            }
        },
        "tabs": ["待付款", "待收货", "拼团中", "已完成", "全部"],
        "deleteIcon": $.getConfig("image_root") + "/trash.png",
        "activeTab": 0
    },
    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的方法列表
     */
    methods: {
        onclick8: function (e) {

            var me = this;
    
            var index = $.toInt(e.currentTarget.dataset.index);
            if (index != me.data.activeTab) {
                me.setData({activeTab: index});
            }
    
        },
        onchange10: function (e) {
    
            var me = this;
    
            me.setData({
                activeTab: e.detail.current
            });
    
    
        },
        onRemove:function(e){
            var me = this;
            var id = $.toInt(e.currentTarget.dataset.id);
            var key=e.currentTarget.dataset.key;
            $.confirmBox("询问","确定要删除此订单？",function () {
                $.api("mall.Order.Remove",{id:id})
                    .then(function (value) {
                        me.refresh(key);
                    })
            })
        },
        onPay:function(e){
            var me = this;
            var id = $.toInt(e.currentTarget.dataset.id);
            var key=e.currentTarget.dataset.key;
            $.api("mall.Order.Pay", {
                orderid: id
            })
                .then(function (value) {
                    wx.requestPayment({
                        'timeStamp': value.timeStamp,
                        'nonceStr': value.nonceStr,
                        'package': value.package,
                        'signType': 'MD5',
                        'paySign': value.paySign,
                        'success': function (res) {
                            $.alertBox("提示", "支付成功")
                                .then(function () {
                                    me.refresh(key);
                                    me.refresh("unrec");//刷新已支付的界面
                                })
                        },
                        'fail': function (res) {
                            console.log("支付 res:" + $.toJson(res));
                            //支付 res:{"err_code":"-1","err_desc":"调用支付JSAPI缺少参数: package","errMsg":"requestPayment:fail"}
                            //$.errorBox("错误", "支付失败：" + res.errMsg + " 原因：" + res.err_desc);
                            wx.showToast({
                                title: '支付失败',
                                icon: 'error',
                                duration: 2000,
                                success:function () {
    
                                }
                            })
                        },
                        'complete': function (res) {
                            //$.back();
                        }
                    })
                });
        },
        onScrollViewUpload_all: function (e) {
            var me = this;
            me.loadNextPage("all");
        },
        onScrollViewDownRefresh_all: function (e) {
            var me = this;
            me.refresh("all");
        },
        onScrollViewUpload_unpay: function (e) {
            var me = this;
            me.loadNextPage("unpay");
        },
        onScrollViewDownRefresh_unpay: function (e) {
            var me = this;
            me.refresh("unpay");
        },
        onScrollViewUpload_unrec: function (e) {
            var me = this;
            me.loadNextPage("unrec");
        },
        onScrollViewDownRefresh_unrec: function (e) {
            var me = this;
            me.refresh("unrec");
        },
        onScrollViewUpload_finish: function (e) {
            var me = this;
            me.loadNextPage("finish");
        },
        onScrollViewDownRefresh_finish: function (e) {
            var me = this;
            me.refresh("finish");
        },
        onScrollViewUpload_cancel: function (e) {
            var me = this;
            me.loadNextPage("cancel");
        },
        onScrollViewDownRefresh_cancel: function (e) {
            var me = this;
            me.refresh("cancel");
        },
        refresh: function (cat) {

            var me = this;
            me.data.meta[cat].cond.pageindex = 0;
            me.loadNextPage(cat);
        },
    
        loadNextPage: function (cat) {
    
            var me = this;
            var cond = me.data.meta[cat].cond;
    
            cond.pageindex++;
            if (cond.pageindex === 1) me.data.meta[cat].datas = []; //第一页要重置
            if( cond.pageindex <= me.data.meta[cat].totalpage ){
                $.api("mall.Order.ListPage", cond)
                    .then(function (value) {
                        //$.log("加载到数据 "+cat+":"+$.toJson(value.datas));
                        var datas = me.data.meta[cat].datas;
                        datas = datas.concat(value.datas);
                        var obj={};
                        obj["meta."+cat+".datas"] = datas;
                        obj["meta."+cat+".totalpage"] = value.totalpage;
                        me.setData(obj);
    
                    }, function (reason) {
                        console.log(reason.message);
                    })
            }
    
        }
    },
    lifetimes: {
        // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
        attached: function () {
            var me = this;

            me.setData({ activeTab: $.toInt(getApp().globalData.order_statuscat)});
        me.refresh("all");
        me.refresh("unpay");
        me.refresh("unrec");
        me.refresh("finish");
        me.refresh("inpin");
        },
        detached: function () {
            var me = this;

        },
    },
})