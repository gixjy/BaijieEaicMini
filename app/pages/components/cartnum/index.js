// pages/components/cartnum/index.js
//本组件实现购物车数量加减操作
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        value:{
            type:Number,
            value:1
        },
        minValue:{
            type:Number,
            value:1
        },
        maxValue:{
            type:Number,
            value:100
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        num:0
    },
    options: {
        addGlobalClass: true,
    },
    observers:{
        "value":function (v) {
            var me = this;
            me.setData({num:v})
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onClickSub: function (e) {
            var me = this;

            if (me.data.num > me.properties.minValue) {
                var v = me.data.num-1;
                me.setData({
                    num: v
                });
                me.triggerEvent("change", {value:v}, {});
            }
        },
        onClickAdd: function (e) {
            var me = this;

            if (me.data.num < me.properties.maxValue) {
                var v = me.data.num+1;
                me.setData({
                    num: v
                });
                me.triggerEvent("change", {value:v}, {});
            }
        }
    }
});
