var $ = getApp().globalData.eui;
var biz = require("../../../biz.js");
//本组件实现sku的选择器功能
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        //可供选择的sku
        //[{id,sn,icon,saleprice,marketprice,choices:[{choiceid,propid,skuid},...]},...]
        skus: {
            type: Array,
            value: null
        },
        //所有的选项
        //[{id,name,style,choices:[{id,name,url},...]},...]
        skuprops:{
            type:Array,
            value:null
        }
    },
    skutool : null,

    /**
     * 组件的初始数据
     */
    data: {
        propArr:[],
        curSku:null
    },
    options: {
        addGlobalClass: true,
    },
    observers:{
        "skus,skuprops" : function (skus,skuprops) {
            var me = this;
            if( !me.skutool ) me.skutool = biz.SkuTool.create();
            if( $.isArray(skus)) me.skutool.Skus = skus;
            if( $.isArray(skuprops)) {
                me.skutool.Props = skuprops;
                //克隆一份到propArr
                var arr=[];
                $.each(skuprops,function (sp) {
                    var p = {};
                    p.id = sp.id;
                    p.name = sp.name;
                    p.style = sp.style;
                    p.choices=[];
                    $.each(sp.choices,function (c) {
                        var c2 ={};
                        c2.id = c.id;
                        c2.name = c.name;
                        c2.url = c.url;
                        p.choices.push(c2);
                    });
                    arr.push(p);
                });
                me.setData({"propArr":arr});
            }

            var curSku = me.skutool.findLowest();
            if( curSku ) {
                $.log("=--找到最低："+$.toJson(curSku));

                me.switchSku(curSku);

                var choices=[];
                $.each(curSku.choices,function (choice) {
                    choices.push(choice.choiceid);
                });
                me.activeChoices(choices);
            }
        }
    },
    /**
     * 组件的方法列表
     */
    methods: {
        onSelChoice:function (e) {
            var me = this;

            var propid = $.toInt(e.currentTarget.dataset.propid);
            var curChoiceid = $.toInt(e.currentTarget.dataset.id);

            //收集所有选中的choice
            var choices=[];
            $.each(me.data.propArr,function (prop) {
                if( propid === $.toInt(prop.id)){
                    choices.push(curChoiceid);
                }else{
                    $.each(prop.choices,function (c) {
                        if( c.selected ){
                            choices.push(c.id);
                            return true;
                        }
                    });
                }
            });

            var status = me.skutool.formatChoices(choices,curChoiceid,me.data.curSku);

            if( status.sku !== me.data.curSku ){
                me.switchSku(status.sku);
            }
            me.activeChoices(status.choices);
        },


        //切换到sku
        switchSku:function (sku) {
            var me = this;
            me.setData({
                curSku:sku
            });
            if( sku ){
                //触发事件
                me.triggerEvent("change", {sku:sku}, {});
            }
        },
        //激活选中的choice,更新选中状态
        activeChoices:function (choices) {
            var me = this;
            var curSku = me.data.curSku;
            $.each(me.data.propArr,function (prop) {
                $.each(prop.choices,function (c) {
                    c.selected = ($.indexOfArray(c.id,choices,function (a,b) {
                        return $.toInt(a) === $.toInt(b)
                    }) >= 0 );
                    /*if( c.selected )
                        c.disabled = false;
                    else{
                        if( curSku ){
                            c.disabled = ($.indexOfArray(c.id,curSku.choices,function (a,b) {
                                return $.toInt(a)===$.toInt(b.choiceid)
                            })<0);
                        }else{
                            c.disabled = true;
                        }
                    }*/
                })
            });
            me.setData({propArr:me.data.propArr});
        }
    },
    lifetimes:{
        created:function () {
            var me = this;
            //do nothing
        }
    }
});
