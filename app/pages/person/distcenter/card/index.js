var $ = getApp().globalData.eui;
$.page({

    /**
     * 页面的初始数据
     */
    data: {
        cards: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var me = this;

        me.refresh();

    },
    notifyWithdrawCardChange: function () {
        this.refresh();
    },
    refresh: function () {
        var me = this;

        var fnGroupStr = function (str, num) {
            if (str == null || str == undefined) return [];
            if (!(/^[0-9]*[1-9][0-9]*$/.test(num))) return [];
            var array = new Array();
            var len = str.length;
            for (var i = 0; i < (len / num); i++) {
                if ((i + 1) * num > len) {
                    array.push(str.substring(i * num, len));
                } else {
                    array.push(str.substring(i * num, (i + 1) * num));
                }
            }
            return array;
        }

        $.api("mall.WithdrawCard.LoadAll", {})
            .then(function (value) {
                value.cardno = fnGroupStr($.toString(value.cardno), 4).join(" ");
                me.setData({
                    "cards": value
                });
            }, function (err) {
                $.errorBox("错误", err.message)
            })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    onEdit: function (e) {
        $.go("/pages/person/distcenter/card/edit?id=" + e.currentTarget.dataset.id);
    },
    onAdd: function (e) {
        $.go("/pages/person/distcenter/card/edit");
    },
    onRemove: function (e) {
        var me = this;
        $.confirmBox("询问", "确定要删除吗？", function () {
            $.api("mall.WithdrawCard.Remove", {
                    "id": e.currentTarget.dataset.id
                })
                .then(function (value) {
                    me.refresh();
                }, function (err) {
                    $.errorBox("错误", err.message)
                })
        })
    }
})