var $ = getApp().globalData.eui;
$.page({

    /**
     * 页面的初始数据
     */
    data: {
        title: "",
        money: '',
        id: 0,
        meta: {
            cardno: "",
            name: "",
            bankname: "",
            openbank: ""
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var me = this;
        var id = $.toInt(options.id);
        me.setData({
            "id": id,
            "title": (id ? "修改" : "添加") + "银行卡"
        });
        if (id) {
            $.api("mall.WithdrawCard.Load", {
                    "id": id
                })
                .then(function (value) {
                    var obj = {};
                    $.each(me.data.meta, function (v, k) {
                        obj[k] = value[k]
                    });
                    me.setData({
                        "meta": obj
                    });
                }, function (err) {
                    $.errorBox("错误", err.message);
                })
        }
    },

    onSubmit: function (e) {
        var me = this;
        $.api("mall.WithdrawCard." + (me.data.id ? "Update" : "Add"), me.data.meta)
            .then(function (value) {
                $.alertBox("提示", "提交成功").then(function () {
                    e.done();
                    me.notify("WithdrawCardChange", []);
                    $.back();
                })
            }, function (reason) {
                $.alertBox("提示", "" + reason.message).then(function () {
                    e.done();
                })
            })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})