var $ = getApp().globalData.eui;
$.page({

  /**
   * 页面的初始数据
   */
  data: {
    meta: {
      cond: {
        pageindex: 1,
        pagesize: 8,
        level: 1
      },
      datas: [],
      totalpage: 0,
      totalcount: 0
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var me = this;
    me.refresh();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onScrollViewUpload: function (e) {

    var me = this;

    me.loadNextPage();

  },
  onScrollViewDownRefresh: function (e) {

    var me = this;

    me.refresh();

  },
  refresh: function () {

    var me = this;
    me.data.meta.cond.pageindex = 0;
    me.loadNextPage();
  },
  loadNextPage: function () {

    var me = this;
    var cond = me.data.meta.cond;
    cond.level = me.data.level;


    cond.pageindex++;
    if (cond.pageindex === 1) me.data.meta.datas = []; //第一页要重置

    $.api("mall.User.ListPageCustomer", cond)
        .then(function (value) {
          var p = me.data.meta.datas;


          if ($.count(value.datas) > 0)
            p = p.concat(value.datas);
          me.setData({
            "meta.datas": p,
            "meta.totalpage": value.totalpage,
            "meta.totalcount": value.totalcount
          });

        }, function (reason) {
          console.log(reason.message);
        })

  }
})