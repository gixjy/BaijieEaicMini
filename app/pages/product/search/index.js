// pages/product/search/index.js
var $ = getApp().globalData.eui;
$.page({

	/**
	 * 页面的初始数据
	 */
	data: {
		"meta": {
			"datas": [],
			"cond": {
				search_text:"",
				"pageindex": 1,
				"pagesize": 10
			}
		},

	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	},
	onSearch:function (e) {
		var me = this;
		$.log("--- onSearch:"+me.data.meta.cond.search_text);

		me.refresh()
	},
	loadNextPage: function () {

		var me = this;
		var cond = me.data.meta.cond;
		cond.pageindex++;
		if (cond.pageindex === 1) me.data.meta.datas = []; //第一页要重置
		$.api("mall.Product.ListPage", cond)
			.then(function (value) {
				var datas = me.data.meta.datas;
				if ($.count(value.datas) > 0) datas = datas.concat(value.datas);
				me.setData({
					"meta.datas": datas
				});

			}, function (reason) {
				console.log(reason.message);
			})

	},
	refresh: function () {

		var me = this;
		me.setData({
			"meta.cond.pageindex":0
		});
		me.loadNextPage();
	},
	onScrollViewUpload1: function (e) {

		var me = this;

		me.loadNextPage();

	},
	onScrollViewDownRefresh1: function (e) {

		var me = this;

		me.refresh();

	},
	onViewProduct: function (e) {

		var me = this;

		$.go("/pages/product/detail?id=" + e.currentTarget.dataset.id);
		//$.go("/pages/product/detail?scene=dist:" + e.currentTarget.dataset.id+"_1");

	},
})