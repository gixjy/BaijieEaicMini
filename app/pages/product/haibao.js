var $ = getApp().globalData.eui;
$.page({
	data: {
		"vwidth": 1,
		"vheight": 1,
		"id": 0,
		"minifile": "",
		"qrCodePath": "",
		"product": {},
		"canvasid": "myCanvas",
		"cansubmit": false,
		"showSharePic": true,
		"shareImage": "",
		"productIcon": "",
		haibaoWidth:0,
		haibaoHeight:0
	},
	onLoad: function (options) {

		var me = this;

		var haibao = getApp().globalData.haibao;
		var entities = $.fromJson(haibao.content);
		me.setData({
			"haibaoWidth" : haibao.pageWidth,
			"haibaoHeight":haibao.pageHeight
		});

		me.drawHaibao(haibao.pageWidth,haibao.pageHeight,entities,function () {

		})



	},
	onclick2: function (e) {

		var me = this;

		wx.showToast({
			title: '正在生成图片...',
			mask: true,
		});
		//y方向的偏移量，因为是从上往下绘制的，所以y一直向下偏移，不断增大。
		let y = 20;
		const goodsTitle = me.data.product.intro;
		let goodsTitleArray = [];
		//为了防止标题过长，分割字符串,每行22个，限制最多5行
		for (let i = 0; i < goodsTitle.length / 22 && i < 5; i++) {
			if (i > 2) {
				break;
			}
			goodsTitleArray.push(goodsTitle.substr(i * 22, 22));
		}
		//const price = this.data.orderDetail.price;
		//const marketPrice = this.data.orderDetail.marketPrice;
		//const title1 = '您的好友邀请您一起分享精品好货';
		//const title2 = '立即打开看看吧';
		//const codeText = '长按识别小程序码查看详情';
		//const imgWidth = 780;
		//const imgHeight = 1600;

		var canvasID = me.data.canvasid;

		const canvasCtx = wx.createCanvasContext(canvasID);
		//绘制背景
		canvasCtx.setFillStyle('#e67a2c');
		canvasCtx.fillRect(0, 0, 390, 800);

		//绘制头像
		if (me.data.usericon) {
			canvasCtx.save(); // 先保存状态 已便于画完圆再用
			canvasCtx.beginPath(); //开始绘制
			//先画个圆
			canvasCtx.setFillStyle('white'); //'transparent');
			canvasCtx.arc(10 + 16, y + 16, 16, 0, Math.PI * 2, false);
			canvasCtx.fill();
			canvasCtx.setStrokeStyle('white');
			// 画出当前路径的边框
			canvasCtx.stroke();
			// ctx.stroke()
			canvasCtx.closePath();
			canvasCtx.clip(); //画了圆 再剪切  原始画布中剪切任意形状和尺寸。一旦剪切了某个区域，则所有之后的绘图都会被限制在被剪切的区域内

			canvasCtx.setFillStyle('white'); //'transparent');
			canvasCtx.arc(10 + 16, y + 16, 16, 0, Math.PI * 2, false);

			canvasCtx.drawImage(me.data.usericon, 10, y, 32, 32);
			canvasCtx.restore(); //恢复之前保存的绘图上下文 恢复之前保存的绘图上下午即状态 可以继续绘制
		}

		//绘制昵称
		/*canvasCtx.setFontSize(14);
		canvasCtx.setFillStyle('#eceeee');
		canvasCtx.setTextAlign('left');
		canvasCtx.fillText(me.data.rental.createuser.alias, 50, y+20);
		canvasCtx.stroke();*/
		y += 60;

		//绘制商品主图
		canvasCtx.drawImage(me.data.productIcon, 10, y, 360, 200);
		y += 240;

		//绘制标题
		if (me.data.product.name) {
			canvasCtx.setFontSize(16);
			canvasCtx.setFillStyle('#fbf8ff');
			canvasCtx.setTextAlign('left');
			canvasCtx.fillText(me.data.product.name, 10, y);
			canvasCtx.stroke();
		}

		y += 30;

		//绘制地址
		/*canvasCtx.setFontSize(14);
		canvasCtx.setFillStyle('#dedede');
		canvasCtx.setTextAlign('left');
		canvasCtx.fillText('地址：'+me.data.rental.locationname, 10, y);
		canvasCtx.stroke();
		y+= 20;

		//绘制户型
		canvasCtx.setFontSize(14);
		canvasCtx.setFillStyle('#dedede');
		canvasCtx.setTextAlign('left');
		canvasCtx.fillText('户型：'+me.data.fuxin_arr[$.toInt(me.data.rental.fuxin)], 10, y);
		canvasCtx.stroke();
		y+= 20;

		//绘制月租
		canvasCtx.setFontSize(14);
		canvasCtx.setFillStyle('#010201');
		canvasCtx.setTextAlign('left');
		canvasCtx.fillText('月租：￥'+ $.toInt(me.data.rental.fee), 10, y);
		canvasCtx.stroke();
		y+= 20;*/

		//绘制内容
		y += 20;
		canvasCtx.setFontSize(14);
		canvasCtx.setFillStyle('white');
		canvasCtx.setTextAlign('left');
		var t = y;
		$.each(goodsTitleArray, function (text) {
			canvasCtx.fillText(text, 10, t);
			t += 20;
		});

		canvasCtx.stroke();
		y += 100;

		//绘制底部背景
		y += 20;
		canvasCtx.setFillStyle('white');
		canvasCtx.fillRect(0, y, 390, 800 - y);

		//绘制广告语
		y += 50;
		canvasCtx.setFontSize(22);
		canvasCtx.setFillStyle('#e67a2c');
		canvasCtx.setTextAlign('center');
		canvasCtx.fillText('广州艾丽星进口红酒洋酒私房菜', 190, y);
		canvasCtx.stroke();
		y += 30;

		//绘制小程序码
		canvasCtx.drawImage(me.data.qrCodePath, 133, y, 124, 124);
		y += 124 + 20;

		//操作提示
		canvasCtx.setFontSize(16);
		canvasCtx.setFillStyle('#333');
		canvasCtx.setTextAlign('center');
		canvasCtx.fillText('长按识别，查看商品详情', 190, y);
		canvasCtx.stroke();

		//canvasCtx.draw();





	},
	drawHaibao:function (width,height,entities,fnOK) {
		var me = this;
		var canvasID = me.data.canvasid;

		const canvasCtx = wx.createCanvasContext(canvasID);

		canvasCtx.setFillStyle('white');
		canvasCtx.fillRect(0, 0, width, height);

		$.each(entities,function (entity) {
			if( entity.type==='text'){
				var text = entity.text;
				var fontsize = entity.font.size;
				canvasCtx.setFontSize(fontsize);
				canvasCtx.setFillStyle(entity.font.color);

				if( text.length > 22 ){
					var y = entity.top+fontsize;

					const goodsTitle = text;
					let goodsTitleArray = [];
					//为了防止标题过长，分割字符串,每行22个，限制最多5行
					for (let i = 0; i < goodsTitle.length / 22 && i < 5; i++) {
						if (i > 2) {
							break;
						}
						goodsTitleArray.push();
						canvasCtx.fillText(goodsTitle.substr(i * 22, 22), entity.left, y);
						y += fontsize+10;
					}
				}else{

					//canvasCtx.setTextAlign('center');
					canvasCtx.fillText(text, entity.left, entity.top+entity.font.size);
				}

				canvasCtx.stroke();
			}else if( entity.type==='image'){
				canvasCtx.drawImage(entity.$path, 0,0,entity.$width,entity.$height,entity.left, entity.top, entity.width,entity.height);
			}
		});

		//保存到相册
		var fnSave = function () {
			wx.saveImageToPhotosAlbum({
				filePath: me.data.shareImage,
				success(res) {
					$.log("图片已保存到相册，赶紧晒一下吧~");
					/*wx.showModal({
						content: '图片已保存到相册，赶紧晒一下吧~',
						showCancel: false,
						confirmText: '好哒',
						confirmColor: '#72B9C3',
						success: function (res) {
							if (res.confirm) {
								console.log('用户点击确定');

							}

						}
					});*/
				}
			});
		};

		canvasCtx.draw(false, function () {
			setTimeout(function () {
				wx.canvasToTempFilePath({
					canvasId: canvasID,
					success: function (res) {
						setTimeout(function () {
							var tempFilePath = res.tempFilePath;
							console.log("生成的图片临时目录：" + tempFilePath);
							me.setData({
								shareImage: tempFilePath,
								showSharePic: false
								// canvasHidden:true
							});
							wx.hideToast();

							wx.previewImage({
								current: tempFilePath, // 当前显示图片的http链接
								urls: [tempFilePath] // 需要预览的图片http链接列表
							});

							$.isAuthed("scope.writePhotosAlbum").then(function (isAuthed) {
								if (!isAuthed) {
									wx.authorize({
										scope: 'scope.writePhotosAlbum',
										success: function (res) {
											fnSave();
										}
									});
								} else {
									fnSave();
								}
							})
						}, 2000);
					},
					fail: function (res) {
						console.log(res);
						wx.hideToast()
					}
				})
			}, 200);
		});
	}
});