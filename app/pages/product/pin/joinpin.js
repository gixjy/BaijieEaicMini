var $ = getApp().globalData.eui;
var biz = require("../../../biz.js");
var Decimal = require("../../../decimal.js");
$.page({
    data: {
        "addrid": 0,
        "recvaddr": {},
        "product": {},
        "sku": {},
        "pin":{},
        "productid": 0,
        "icon": "",
        "price": "",
        "pinid":0,
        "num" : 1,
        shipfee:0,
        totalprice:0
    },
    onLoad: function (options) {

        var me = this;
        var skuid = $.toInt(options.skuid);
        var pinid = $.toInt(options.pinid);

        $.api("mall.Pin.Load",{"id":pinid})
            .then(function (oPin) {
                oPin.pintype = $.toInt(oPin.pintype);
                oPin.pincount = $.toInt(oPin.pincount);
                oPin.maxcount =$.toInt(oPin.maxcount);
                oPin.pinprice = $.toFloat(oPin.pinprice);
                oPin.saleprice = $.toFloat(oPin.saleprice);
                oPin.shenprice = ""+new Decimal(oPin.saleprice).minus(oPin.pinprice);


                $.api("mall.Sku.Load",{id:oPin.skuid})
                    .then(function (oSku) {
                        $.api("mall.Product.Load",{id:oSku.productid})
                            .then(function (oProduct) {
                                var tool = biz.SkuPriceTool.create(oSku,1);
                                var shipfee = tool.getShipFee();
                                me.setData({
                                    "pinid":pinid,
                                    "pin":oPin,
                                    "sku" : oSku,
                                    shipfee:shipfee,
                                    totalprice:$.toFloat(oPin.pinprice+shipfee).toFixed(2),
                                    "product" : oProduct
                                })
                            })
                    })
            });
        $.api("mall.RecvAddress.LoadDefault", {}).then(function (value) {
            if (value && $.isObject(value)) {
                me.setData({
                    "addrid": value.id,
                    "recvaddr": value
                });
            }
        });


    },
    onNumChange:function(e){
        var me = this;
        var num=e.detail.value;

        var tool = biz.SkuPriceTool.create(me.data.sku,num);
        var shipfee = tool.getShipFee();

        var totalprice = $.toFloat(me.data.pin.pinprice*num+shipfee).toFixed(2);
        me.setData({
            num : num,
            shipfee:shipfee,
            totalprice:totalprice
        });

    },
    onClickSelAddr: function (e) {
        var me = this;
        $.go("/pages/person/recvaddress/selrecvaddr");
    },

    onStartPay: function (e) {

        var me = this;

        if (!me.data.addrid) {
            $.errorBox("错误", "请选择收货地址", function () {
            });
            return;
        }

        $.api("mall.Order.JoinPin", {
            productid: me.data.product.id,
            skuid:me.data.sku.id,
            addrid: me.data.addrid,
            pinid:me.data.pinid,
            num:me.data.num
        })
            .then(function (value) {
                //$.alertBox("提示","提交成功");
                $.api("mall.Order.Pay", {
                    orderid: value
                })
                    .then(function (value) {
                        wx.requestPayment({
                            'timeStamp': value.timeStamp,
                            'nonceStr': value.nonceStr,
                            'package': value.package,
                            'signType': 'MD5',
                            'paySign': value.paySign,
                            'success': function (res) {
                                $.alertBox("提示", "支付成功")
                                    .then(function () {
                                        $.back();
                                    })
                            },
                            'fail': function (res) {
                                console.log("支付 res:" + $.toJson(res));
                                //支付 res:{"err_code":"-1","err_desc":"调用支付JSAPI缺少参数: package","errMsg":"requestPayment:fail"}
                                if (res.errMsg.indexOf("fail cancel")) {
                                    $.alertBox("提示", "支付取消");
                                } else {
                                    $.errorBox("错误", "支付失败：" + res.errMsg + " 原因：" + res.err_desc);
                                }

                            },
                            'complete': function (res) {
                                //$.back();
                            }
                        })
                    });
            })

    },
    notifySelRecvAddress: function (addr) {

        var me = this;
        me.setData({
            addrid: addr.id,
            recvaddr: addr
        });
    }
});