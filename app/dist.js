var dist={};

var eui=null;
dist.init=function($){
	eui=$;
	return dist;
};

dist.config=null;
dist.getConfig=function(fnOK,fnFail){
	if( !dist.config ) {
		$.api("site.Setting.DistConfigMini",{})
			.then(function (oConfig) {
				dist.config = oConfig;
				fnOK(dist.config);
			},function (err) {
				if($.isFunction(fnFail)) fnFail(err);
			})
	}else{
		fnOK(dist.config);
	}
}

module.exports=dist;